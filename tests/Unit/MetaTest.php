<?php

namespace Tests\Unit;

use App\Models\Competition;

class MetaTest extends TestCase
{
	/**
		@brief		Try some normal meta.
		@since		2018-04-08 02:16:12
	**/
	public function test_normal_meta()
	{
		$competition = new Competition();
		$competition->key = $competition->get_new_key();
		$competition->save();

		$competition->meta()->set( 'hello1', 'hello2' );

		$this->assertDatabaseHas( 'meta', [
			'object_type' => 'Competition',
			'object_id' => '1',
			'meta_key' => 'hello1',
			'meta_value' => 'hello2',
		] );

		$this->assertEquals( 'hello2', $competition->meta()->get( 'hello1' ) );
	}

	/**
		@brief		Now try with some json.
		@since		2018-04-08 02:28:24
	**/
	public function test_json_meta()
	{
		$competition = new Competition();
		$competition->key = $competition->get_new_key();
		$competition->save();

		$competition->meta()->set( 'jsontest', [ 1, 2, 3 ] );

		$this->assertEquals( [ 1, 2, 3 ], $competition->meta()->get_json( 'jsontest' ) );
	}
}
