<?php

namespace Tests\Unit;

use Tests\TestCase as LaravelTestCase;
//use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class TestCase extends LaravelTestCase
{
	use DatabaseMigrations;
}
