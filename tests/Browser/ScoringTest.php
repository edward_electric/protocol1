<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

/**
	@brief		Test scoring
	@group		scoring
	@since		2018-09-16 22:10:52
**/
class ScoringTest extends DuskTestCase
{
	/**
		@brief		Test scoring a little.
		@since		2018-10-06 12:10:09
	**/
	public function test_some_scoring()
	{
		$browser = $this->register_someone();
		$browser->back();

		for( $participant_count = 1; $participant_count < 2; $participant_count++ )
		{
			// Register someone else.
			$registration = new Components\Registration( $browser );
			$registration->register( $browser, substr( md5( microtime() ), 0, 12 ) );
			$browser->back();
			$browser->pause( 500 );
		}

		// Get the competition ID.
		$competition_id = $browser->driver->getCurrentURL();
		$competition_id = preg_replace( '/.*\//', '', $competition_id );

		$browser->click( 'button.scoring' );

		$browser->pause( 500 );

		// to 2
		$browser->click( '.next_lane button' );
		$browser->pause( 500 );
		// Back to 1
		$browser->click( '.previous_lane button' );
		$browser->pause( 500 );

		$input_precision_total = 'input.precision_total';
		$browser->click( $input_precision_total );
		$browser->value( $input_precision_total, '12' );

		// We have to be very precise here since there could be more than one save button hanging around.
		$save_button = '#competition_scoring .precision_score .save_button button';
		$browser->assertMissing( $save_button );

		for( $counter = 1; $counter < 12; $counter++ )
		{
			for( $participants = 0; $participants < $participant_count; $participants++ )
			{
				$browser->pause( 500 );

				$series_text = 'serie ' . $counter;
				$browser->assertSee( $series_text );

				$score = 0;
				for( $shot = 1; $shot < 6; $shot++ )
				{
					$shot_score = rand( 0, 10 );
					$score += $shot_score;
					$browser->keys( 'input.shot_' . $shot, $shot_score );
				}

				$browser->click( $input_precision_total );
				$browser->assertValue( $input_precision_total, $score );

				$browser->pause( 500 );

				$browser->click( $save_button );
			}
		}

		// Wait for lane change.
		$browser->pause( 500 );

		$save_button = '#competition_scoring .precision_score .save_and_finish button';
		$browser->assertMissing( $save_button );

		for( $participants = 0; $participants < $participant_count; $participants++ )
		{
			$browser->assertSee( 'serie 12' );

			$score = 0;
			for( $shot = 1; $shot < 6; $shot++ )
			{
				$shot_score = rand( 0, 10 );
				$score += $shot_score;
				$browser->keys( 'input.shot_' . $shot, $shot_score );
			}

			$browser->assertValue( $input_precision_total, $score );

			$browser->click( $input_precision_total );

			$browser->pause( 500 );

			$browser->click( $save_button );

			$browser->pause( 500 );
		}
	}
}
