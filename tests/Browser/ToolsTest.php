<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

/**
	@brief		Test the tools.
	@since		2018-09-16 22:10:52
**/
class ToolsTest extends DuskTestCase
{
    /**
    	@brief		Test the timer.
    	@since		2018-09-16 22:11:00
    **/
    public function test_timer()
    {
		$browser = $this->start_competition();

		$browser->click( 'button.tools' );

		$browser->assertSee( 'Stoppur' );

		$browser->click( '.timer fieldset.collapsible legend' );

		// Click all of the buttons.
		$browser->click( '.timer .time.button[data-time="150"]' );
		$browser->click( '.timer .time.button[data-time="20"]' );
		$browser->click( '.timer .time.button[data-time="15"]' );
		$browser->click( '.timer .time.button[data-time="10"]' );

		// Start a short timer.
		$browser->click( '.timer .start.button' );
		$browser->assertSee( '10 sekunder kvar' );
		$browser->pause( 7000 );
		$browser->assertSee( 'Färdiga' );
		$browser->pause( 3000 );
		$browser->assertSee( 'Eld!' );
		$browser->pause( 7000 );
		$browser->assertSee( 'Eld upphör' );
    }
}
