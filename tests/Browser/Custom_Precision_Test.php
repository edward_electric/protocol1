<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;

/**
	@brief		Test custom precision series.
	@group		custom_precision
	@since		2019-01-01 11:47:21
**/
class Custom_Precision_Test extends DuskTestCase
{
    use DatabaseMigrations;
	use RefreshDatabase;

	/**
		@brief		Test custom precision series.
		@since		2019-01-01 11:47:37
	**/
	public function test_custom_precision()
	{
    	$browser = $this->start_competition();
    	// Visit the settings.
    	$browser->click( 'button.settings_index' );
    	$browser->click( 'button.series' );

    	// Delete the existing series.
    	for( $counter = 0; $counter < 11; $counter++ )
    		$browser->click( '.current.series button.remove' );

    	$browser->click( '.add_new_series .save_button button' );

    	// Add a new series.
    	$browser->value( '.input_shots', 8 );
    	$browser->value( '.input_time', 61 );

    	$browser->click( '.add_new_series .save_button button' );

    	$browser->pause( 500 );

    	$browser->click( '.add_new_series button[data-type="precision"]' );

    	$browser->pause( 500 );

    	$browser->click( '.add_new_series .save_button button' );

    	$browser->back();
    	$browser->back();

    	// Register someone.
		$registration = new Components\Registration( $browser );
		$registration->register( $browser, substr( md5( microtime() ), 0, 12 ) );

    	$browser->back();

    	// Mark
    	$browser->click( 'button.scoring' );

		$input_precision_total = 'input.precision_total';

    	// The first series should contain 5 inputs.
    	$browser->assertMissing( 'input.shot_6' );
		$browser->keys( 'input.shot_1', "4" );
		$browser->keys( 'input.shot_2', "6" );
		$browser->keys( 'input.shot_3', "7" );
		$browser->keys( 'input.shot_4', "8" );
		$browser->keys( 'input.shot_5', "9" );

		$browser->assertValue( $input_precision_total, 4 + 6 + 7 + 8 + 9 );

		$browser->click( '.precision_score .save_button button' );

		$browser->pause( 250 );

		$browser->keys( 'input.shot_1', "4" );
		$browser->keys( 'input.shot_2', "6" );
		$browser->keys( 'input.shot_3', "7" );
		$browser->keys( 'input.shot_4', "8" );
		$browser->keys( 'input.shot_5', "1" );
		$browser->keys( 'input.shot_6', "2" );
		$browser->keys( 'input.shot_7', "3" );
		$browser->keys( 'input.shot_8', "4" );
		$browser->assertValue( $input_precision_total, 4 + 6 + 7 + 8 + 1 + 2 + 3 + 4 );

		$browser->pause( 250 );

		$browser->click( '.precision_score .save_and_finish button' );

		// Allow the browser some time to shut down, else it kills the db too early.
		$browser->pause( 250 );
	}
}
