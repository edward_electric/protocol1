<?php

namespace Tests\Browser\Pages;

use Laravel\Dusk\Browser;

class Start extends Page
{
    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return '/';
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertPathIs( $this->url() );
    }

    /**
    	@brief		Create a new competition.
    	@since		2018-10-06 12:49:34
    **/
    public function start( Browser $browser )
    {
		$browser->click( '.submit.input_run' );
		return $browser;
    }
}
