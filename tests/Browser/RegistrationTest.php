<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

/**
	@brief		Test registration
	@group		registration
	@since		2018-09-16 22:10:52
**/
class RegistrationTest extends DuskTestCase
{
	/**
		@brief		Test adding a participant.
		@since		2018-09-17 08:38:55
	**/
	public function test_add()
	{
		$browser = $this->start_competition( true );

		$browser->click( 'button.registration' );
		$browser->assertSee( 'Deltagarsnabbval' );
		$browser->assertSee( 'Namn' );

		// The participant quickselect is seeded in database/.
		$browser->select( 'participant_quickselect', 'Edward P' );
		$browser->pause( 250 );
		$browser->assertValue( '.form_item_participant_name input', 'Edward P' );
		$browser->assertValue( '.form_item_participant_class select', 'ML' );
		$browser->assertValue( '.form_item_price select', 'Med pistol 50kr' );

		$all_table = '.html_participants table#all tr td';
		$save_button = 'form#registration .save_button button';

		// Table must be missing.
		$browser->assertMissing( $all_table );
		// Save must be missing.
		$browser->assertMissing( $save_button );

		// Test save button.
		$browser->select( 'lane', '5' );
		$browser->assertVisible( $save_button );

		$browser->click( $save_button );
		$browser->pause( 500 );
		$browser->assertValue( '.form_item_participant_name input', '' );

		// Check that the table has been created.
		$browser->assertVisible( $all_table );

		// Next lane should be selected.
		$browser->assertValue( '.form_item_lane select', '6' );
	}
}
