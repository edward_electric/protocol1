<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class IndexTest extends DuskTestCase
{
    /**
    	@brief		Check stuff of the front page.
    	@since		2018-09-16 22:01:14
    **/
    public function test_view()
    {
		$this->browse(function (Browser $browser)
		{
			$browser->visit( '/' )
				->assertTitleContains( 'Start' )
				->assertSee('TävlingsID')
				->assertSee( 'Logga in' );
		} );
    }

    /**
    	@brief		Test starting a competition.
    	@since		2018-09-16 22:01:38
    **/
    public function test_start_competition()
    {
		$this->browse(function (Browser $browser)
		{
			$browser->visit( '/' );
			$browser->click( '.submit.input_run' );
			$browser->assertPathBeginsWith( '/competition' );
			$browser->assertSee( 'Inställningar' )
				->assertSee( 'Inskrivning' )
				->assertSee( 'Resultat' );
			$browser->assertVisible( 'button.scoring.disabled' );
		} );
    }

}
