<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

/**
	@brief		Test the settings.
	@group		settings
	@since		2018-09-16 22:10:52
**/
class SettingsTest extends DuskTestCase
{
	/**
		@brief		Test changing the quicksetting.
		@since		2018-09-17 12:01:55
	**/
	public function test_quicksetting()
	{
		$browser = $this->start_competition();

		$browser->click( 'button.settings_index' );
		$browser->assertSee( 'Snabbinställningar' );

		$browser->select( '#competition_quicksettings select', 'A mot B mot C' );

		$browser->pause( 500 );
		$browser->assertTitleContains( 'A mot B mot C' );
		$browser->assertValue( '#settings .input_competition_name', 'A mot B mot C' );
	}
}
