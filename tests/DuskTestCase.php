<?php

namespace Tests;

use Laravel\Dusk\TestCase as BaseTestCase;
use Facebook\WebDriver\Chrome\ChromeOptions;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Illuminate\Foundation\Testing\DatabaseMigrations;

abstract class DuskTestCase extends BaseTestCase
{
    use CreatesApplication;
    use DatabaseMigrations;

    /**
    	@brief		The browser instance.
    	@since		2018-10-06 13:34:59
    **/
    public static $instance;

    /**
     * Prepare for Dusk test execution.
     *
     * @beforeClass
     * @return void
     */
    public static function prepare()
    {
        static::startChromeDriver();
    }

    /**
     * Create the RemoteWebDriver instance.
     *
     * @return \Facebook\WebDriver\Remote\RemoteWebDriver
     */
    protected function driver()
    {
        $options = (new ChromeOptions)->addArguments([
            '--disable-gpu',
            '--headless'
        ]);

        return RemoteWebDriver::create(
            'http://localhost:9515', DesiredCapabilities::chrome()->setCapability(
                ChromeOptions::CAPABILITY, $options
            )
        );
    }

	public function setUp()
	{
		parent::setUp();
		$this->artisan( 'db:seed' );
	}

    /**
    	@brief		Start a competition.
    	@since		2018-10-06 13:16:57
    **/
    public function start_competition( $login = false )
    {
		$this->browse( function( $browser )
		{
			static::$instance = $browser;
		} );
		$browser = static::$instance;

		if ( $login )
			$browser->visit( '/admin' )
				->type( 'email', 'edward@plainview.se' )
				->type( 'password', 'opOP1199' )
				->press( 'Logga in' );

		$page = new Browser\Pages\Start();
		$browser->visit( $page );
		$page->start( $browser );
		return static::$instance;
    }

    /**
    	@brief
    	@since		2018-10-06 13:24:07
    **/
    public function register_someone()
    {
    	$browser = $this->start_competition();
		$registration = new Browser\Components\Registration( $browser );
		$registration->register( $browser, substr( md5( microtime() ), 0, 12 ) );
		return $browser;
    }
}
