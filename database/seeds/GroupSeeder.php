<?php

use Illuminate\Database\Seeder;

use App\Models\Group;

class GroupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $g = new Group();
        $g->name = 'Limhamns Skytteförening';
        $g->save();
    }
}
