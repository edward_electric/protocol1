<?php

use Illuminate\Database\Seeder;

use App\Models\Group;
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	// Find the LSF group.
    	$group = Group::firstOrFail();

    	// Add Edward to this group.
    	$user = new User();
    	$user->email = 'edward@plainview.se';
    	$user->name = 'Edward P';
    	$user->password = Hash::make( 'opOP1199' );
    	$user->group()->associate( $group );
    	$user->save();
    }
}
