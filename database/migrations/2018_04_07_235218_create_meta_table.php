<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMetaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'meta', function (Blueprint $table) {
            $table->increments('id');
            $table->string( 'meta_key', 64 );
            $table->longText( 'meta_value' );
            $table->integer( 'object_id' );
            $table->string( 'object_type', 32 );
            $table->timestamps();

            $table->index( 'object_type' );
            $table->index( 'object_id' );
            $table->index( 'meta_key' );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( 'meta' );
    }
}
