@extends( 'layouts.headerfooter' )

@section( 'h1', 'Start' )

@section( 'content' )
	@parent
	{!! $form !!}
@endsection
