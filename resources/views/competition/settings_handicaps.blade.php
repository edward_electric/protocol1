@component( 'parts.panel', [
	'panel_id' => 'competition_settings_handicaps',
	'panel_icon' => 'wheelchair',
	'panel_title' =>  __( "Handikapp" ),
] )

<div class="content">
	<p>{{ __( 'Här kan du ange antal poäng som varje vapengrupp får i handikapp.' ) }}</p>
	{!! $competition_handicaps_form !!}
</div>

@endcomponent
