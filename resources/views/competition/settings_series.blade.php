@component( 'parts.panel', [
	'panel_css' => 'lockable_settings',
	'panel_id' => 'competition_settings_series',
	'panel_icon' => 'calendar-alt',
	'panel_title' =>  __( "Serier" ),
] )

<div class="existing_series_template hidden">
	<div class="existing row">
		<div class="small-2 medium-2 columns series number">
		</div>
		<div class="small-8 medium-9 columns type">
			<div class="series type precision" data-type="precision">
				<i class="fa fa-crosshairs"></i></i>{{ __( "Precision" ) }}<span class="extra_info"></span>
			</div>
			<div class="series type field" data-type="field">
				<i class="fa fa-tree"></i></i>{{ __( "Fält" ) }}<span class="time"></span>
			</div>
		</div>
		<div class="small-2 medium-1 columns remove_button">
			<button class="remove">
				<i class="fa fa-minus-circle"></i></i>
			</button>
		</div>
	</div>
</div>

<div class="content">
	<div class="add_new_series">
		<div class="row">
			<div class="small-12 columns series type">
				<button class="expand" data-type="precision">
					<i class="fa fa-crosshairs"></i></i>{{ __( "Precision" ) }}
				</button>
			</div>
			<div class="small-4 columns series type hidden">
				<button class="expand" data-type="field">
					<i class="fa fa-tree"></i></i>{{ __( "Fält" ) }}
				</button>
			</div>
		</div>
		{!! $settings_series_form !!}
	</div>
	<div class="current series">
	</div>
</div>

@endcomponent
