@component( 'parts.panel', [
	'panel_id' => 'competition_settings_lanes',
	'panel_icon' => 'bars',
	'panel_title' =>  __( "Banor" ),
] )

<div class="content">
	{!! $competition_lanes_form !!}
</div>
<div class="individual_lanes_template hidden">
	<div class="small-3 columns lane textcenter">
		###
	</div>
</div>
<div class="individual_lanes row">
</div>
<div class="save_button_div">
</div>

@endcomponent
