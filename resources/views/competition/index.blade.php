<div id="competition_index" class="p1_panel">

	<div class="competition_name textcenter"></div>

	<div class="button_container">
		<button class="settings_index" data-switch_p1_panel="competition_settings_index">
			<i class="fa fa-cogs"></i></i>{{ __( "Inställningar" ) }}
		</button>
	</div>
	<div class="button_container">
		<button class="registration" data-switch_p1_panel="competition_registration">
			<i class="fa fa-user"></i></i>{{ __( "Inskrivning" ) }}
		</button>
	</div>
	<div class="button_container">
		<button class="scoring disabled" data-switch_p1_panel="competition_scoring">
			<i class="fa fa-pencil-alt"></i></i>{{ __( "Markering" ) }}
		</button>
	</div>
	<div class="button_container">
		<button class="results disabled" data-switch_p1_panel="competition_results">
			<i class="fa fa-trophy"></i></i>{{ __( "Resultat" ) }}
		</button>
	</div>
	<div class="button_container">
		<button class="tools secondary" data-switch_p1_panel="competition_tools">
			<i class="fa fa-wrench"></i></i>{{ __( "Verktyg" ) }}
		</button>
	</div>

	<div class="row">
		<div class="small-6 columns textcenter">
			<a href="{{ route( 'index' ) }}">
				<i class="fa fa-home"></i></i> {{ __( "Startsidan" ) }}
			</a>
		</div>
		<div class="small-6 columns textcenter">
			Denna tävling: <a href="">{{ $competition->key }}</a>
		</div>
	</div>
</div>
