@component( 'parts.panel', [
	'panel_id' => 'competition_scoring',
	'panel_icon' => 'pencil-alt',
	'panel_title' =>  __( "Markering" ),
] )

<div class="hidden">
	@include( 'parts.save_button' )
</div>

<div class="lane_selection row textcenter">
	<div class="small-3 columns previous_lane">
		<button class="icon_is_button">
			<i class="fa fa-hand-point-left"></i></i>
		</button>
	</div>
	<div class="small-6 columns lane_and_participant">
		<span class="lane">x</span>: <span class="participant_name">XXX</span>
	</div>
	<div class="small-3 columns next_lane">
		<button class="icon_is_button">
			<i class="fa fa-hand-point-right"></i></i>
		</button>
	</div>
</div>
<div class="series_selection row textcenter">
	<div class="small-3 columns previous_series">
		<button class="icon_is_button">
			<i class="fa fa-arrow-left"></i></i>
		</button>
	</div>
	<div class="small-6 columns series_number">
		serie <span class="series_number">X</span>
	</div>
	<div class="small-3 columns next_series">
		<button class="icon_is_button">
			<i class="fa fa-arrow-right"></i></i>
		</button>
	</div>
</div>
<div class="precision_score series_type">
	<form class="row collapse textcenter">
		<div class="small-4 large-2 columns single_shot">
			<input type="number" class="shot_1 big_input shot precision_shot focus_me_first" min="0" max="10" step="1" pattern="\d*" placeholder="#1" size="2"></input>
		</div>
		<div class="small-12 columns total">
			<input type="number" class="precision_total big_input" min="0" max="1000" step="1" pattern="\d*" placeholder="###" size="3"></input>
		</div>
		</form>
	<div class="save_button_container">
	</div>
	<div class="row save_and_finish">
		<div class="small-6 columns small-centered medium-4 medium-centered">
			<button class="expand with_icon">
				<i class="fa fa-thumbs-up"></i></i>{{ __( "Spara och avsluta" ) }}
			</button>
		</div>
	</div>
</div>
<div class="field_score series_type">
	<form class="row textcenter">
		<div class="small-6 columns field_hits">
			<input type="number" class="field_hits big_input shot focus_me_first" min="0" max="6" step="1" placeholder="Träffar" size="2"></input>
		</div>
		<div class="small-6 columns field_targets">
			<input type="number" class="field_targets big_input shot" min="0" max="6" step="1" placeholder="Mål" size="2"></input>
		</div>
	</form>
	<div class="save_button_container">
	</div>
	<div class="row save_and_finish">
		<div class="small-6 columns small-centered medium-4 medium-centered">
			<button class="expand with_icon">
				<i class="fa fa-thumbs-up"></i></i>{{ __( "Spara och avsluta" ) }}
			</button>
		</div>
	</div>
</div>

@endcomponent
