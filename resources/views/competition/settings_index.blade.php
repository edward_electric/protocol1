@component( 'parts.panel', [
	'panel_id' => 'competition_settings_index',
	'panel_title' =>  __( "Inställningar" ),
] )

<div class="content">

	{!! $competition_settings_form !!}

	<div class="button_group">
		<div class="button_container">
		<button class="lanes expand" data-switch_p1_panel="competition_settings_lanes">
				<i class="fa fa-bars"></i></i>{{ __( "Banor" ) }} <span class="count"></span>
			</button>
		</div>

		<div class="button_container">
			<button class="prices expand" data-switch_p1_panel="competition_settings_prices">
				<i class="fa fa-dollar-sign"></i></i>{{ __( "Betalningar" ) }} <span class="count"></span>
			</button>
		</div>

		<div class="button_container">
			<button class="handicaps expand" data-switch_p1_panel="competition_settings_handicaps">
				<i class="fa fa-wheelchair"></i></i>{{ __( "Handikapp" ) }} <span class="count"></span>
			</button>
		</div>

		<div class="button_container">
			<button class="classes expand" data-switch_p1_panel="competition_settings_classes">
				<i class="fa fa-star"></i></i>{{ __( "Klasser" ) }} <span class="count"></span>
			</button>
		</div>

		<div class="button_container">
			<button class="series expand" data-switch_p1_panel="competition_settings_series">
				<i class="fa fa-calendar-alt"></i></i>{{ __( "Serier" ) }} <span class="count"></span>
			</button>
		</div>
	</div>

	{!! $competition_quicksettings_form !!}

</div>

@endcomponent
