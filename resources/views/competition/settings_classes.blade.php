@component( 'parts.panel', [
	'panel_css' => 'lockable_settings',
	'panel_id' => 'competition_settings_classes',
	'panel_icon' => 'star',
	'panel_title' =>  __( "Klasser" ),
] )

<div class="content">
	<p>{{ __( 'Ange en förkortning för klassen och sedan en beskrivning.' ) }}</p>
	@include( 'parts.smli.text' )
</div>

@endcomponent
