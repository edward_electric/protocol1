@component( 'parts.panel', [
	'panel_id' => 'competition_results',
	'panel_icon' => 'trophy',
	'panel_title' =>  __( "Resultat" ),
] )

<div class="content">
	<div class="scores_html">
	</div>

	<h3><i class="fa fa-search"></i></i> Detaljerade resultat</h3>
	<div class="detailed_scores_html max_width_100">
	</div>
	<h3><i class="fa fa-dollar-sign"></i></i> {{ __( "Betalningar" ) }}</h3>
	<div class="money_div">
		Det bör finnas <span class="money"></span> kr i kassan.
	</div>
</div>

@endcomponent
