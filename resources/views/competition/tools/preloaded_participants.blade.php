<div class="preloaded_participants">
	<fieldset>
		<legend>Förladdade deltagare <i class="fa fa-upload"></i></legend>
		<div class="contents">
			{!! $preloaded_participants_form !!}
		</div>
	</fieldset>
</div>
