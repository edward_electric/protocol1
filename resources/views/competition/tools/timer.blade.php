<div class="timer">
	<fieldset>
		<legend>Stoppur <i class="fa fa-stopwatch"></i></legend>
		<div class="contents">
			<div class="active">
				<div class="start row">
					<div class="small-12 columns">
						<div class="message_container">
							<div class="message">
								Message
							</div>
						</div>
						<div class="bar_container">
							<div class="bar">
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="setup">
				<ul class="time_select button-group round even-4">
					<li>
						<div class="time button unselected" data-time="10">
							10
						</div>
					</li>
					<li>
						<div class="time button selected" data-time="15">
							15
						</div>
					</li>
					<li>
						<div class="time button unselected" data-time="20">
							20
						</div>
					</li>
					<li>
						<div class="time button unselected" data-time="150">
							150
						</div>
					</li>
				</ul>
				<div class="start row">
					<div class="small-12 columns">
						<div class="start button expand">
							Starta
						</div>
					</div>
				</div>
			</div>
		</div>
	</fieldset>
</div>
