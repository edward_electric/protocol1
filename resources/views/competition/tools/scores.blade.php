<div class="scores">
	<fieldset>
		<legend>Resultatfiler</legend>
		<p>Se på eller ladda hem resultaten i olika format:</p>
		<ul>
			<li><a href="{{ route( 'competition.export.participants.txt', [ 'competition_key' => $competition->key ] ) }}">Deltagande text</a></li>
			<li><a href="{{ route( 'competition.export.results.ods', [ 'competition_key' => $competition->key ] ) }}">Resultat OpenDocument Spreadsheet (ODS)</a></li>
			<li><a href="{{ route( 'competition.export.results.pdf', [ 'competition_key' => $competition->key ] ) }}">Resultat PDF</a></li>
			<li><a href="{{ route( 'competition.export.results.xls', [ 'competition_key' => $competition->key ] ) }}">Resultat Excel (XLS)</a></li>
			<li><a href="{{ route( 'competition.export.results.txt', [ 'competition_key' => $competition->key ] ) }}">Resultat text</a></li>
		</ul>
	</fieldset>
</div>
