@component( 'parts.panel', [
	'panel_id' => 'competition_tools',
	'panel_icon' => 'wrench',
	'panel_title' =>  __( "Verktyg" ),
] )

<div class="tools">
	@include( 'competition.tools.email' )
	@include( 'competition.tools.password' )
	@include( 'competition.tools.scores' )
	@include( 'competition.tools.timer' )
	@includeWhen( isset( $preloaded_participants_form ), 'competition.tools.preloaded_participants' )
</div>

@endcomponent
