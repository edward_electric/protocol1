@component( 'parts.panel', [
	'panel_css' => 'lockable_settings',
	'panel_id' => 'competition_settings_prices',
	'panel_icon' => 'dollar-sign',
	'panel_title' =>  __( "Betalningar" ),
] )

<div class="content">
	<p>{{ __( 'Ange en summa och en beskrivning av betalningsalternativet.' ) }}</p>
	@include( 'parts.smli.number' )
</div>

@endcomponent
