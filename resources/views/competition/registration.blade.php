@component( 'parts.panel', [
	'panel_id' => 'competition_registration',
	'panel_icon' => 'user',
	'panel_title' =>  __( "Inskrivning" ),
] )

<div class="content row">
	<div class="small-12 columns">
		{!! $registration_form !!}
		@include( 'parts.save_button' )
		<div class="html_participants max_width_100">
		</div>
	</div>
</div>

@endcomponent
