<div class="footer row">
	<div class="small-12 medium-4 columns first">
		<a href="{{ route( 'index' ) }}">
			<i class="fa fa-home"></i></i> {{ __( "Startsidan" ) }}
		</a>
	</div>
	<div class="small-12 medium-4 columns middle">
		<div>
			@if ( ! $user = Auth::user() )
				&copy; Protocol1 {{ date( 'Y' ) }}
			@else
				&copy; {{ $user->group->name }} {{ date( 'Y' ) }}
			@endif
		</div>
	</div>
	<div class="small-12 medium-4 columns right">
			@if ( ! Auth::check() )
				<a href="{{ route( 'admin.index' ) }}">Logga in</a>
			@else
				<div>
					<a href="{{ route( 'admin.index' ) }}">Admin</a>
				</div>
				<div>
					<a href="{{ route( 'logout' ) }}">Logga ut</a>
				</div>
			@endif
	</div>
</div>
