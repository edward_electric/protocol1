@hasSection( 'h1' )
	@yield( 'h1' ) | {{ config( 'app.name' ) }}
@else
	{{ config( 'app.name' ) }}
@endif
