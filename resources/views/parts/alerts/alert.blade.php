<div class="row">
	<div class="small-12 columns">
		<div class="alert-box {{ $type }}">
			<div class="message">
				{{ $message }}
			</div>
		</div>
	</div>
</div>
