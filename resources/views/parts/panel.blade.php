<div id="{{ $panel_id }}" class="p1_panel hidden {{ @$panel_css_classes }}" data-title="{{ $panel_title }}">
	<div class="panel_header">
		<h1 class="panel_title">
			@if ( @$panel_icon != '' )
				<i class="fa fa-{{ $panel_icon }}"></i>
			@endif
			{{ $panel_title }}
		</h1>
	</div>

	{{ $slot }}
</div>
