<div class="save_button hidden">
	@include( 'parts.message_box' )
	<div class="row">
		<div class="small-6 columns small-centered medium-4 medium-centered">
			<button class="expand">
				<i class="fa fa-save"></i></i>{{ __( "Spara" ) }}
			</button>
		</div>
	</div>
</div>
