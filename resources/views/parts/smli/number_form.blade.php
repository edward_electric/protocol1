@extends( 'parts.smli.generic_form' )

@section( 'smli_small_input' )
	<input class="input_small_value number" value="" min="0" max="1000" type="number">
@endsection
