<div class="small_and_large_input_template hidden">
	<div class="small_and_large_input row">
		@yield( 'smli_type_form' )
	</div>
</div>
<div class="small_and_large_inputs">
</div>
<div class="add_button_div">
	<div class="row">
		<div class="small-4 columns small-centered textcenter">
			<button class="add icon_is_button" title="{{ __( 'Lägg till en ny rad' ) }}">
				<i class="fa fa-plus-circle"></i></i>
			</button>
		</div>
	</div>
</div>
<div class="save_button_div">
</div>
