<div class="small-6 medium-9 columns large_input">
	<input class="input_large_value text" type="text" value="">
</div>
<div class="small-4 medium-2 columns small_input">
	@yield( 'smli_small_input' )
</div>
<div class="small-2 medium-1 columns remove_button">
	<button title="{{ __( 'Ta bort denna rad' ) }}" class="remove">
		<i class="fa fa-minus-circle"></i></i>
	</button>
</div>
