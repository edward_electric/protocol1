@extends( 'layouts.headerfooter' )

@section( 'h1', 'Logga in' )

@section( 'content' )
	@parent
	{!! $login_form !!}
@endsection
