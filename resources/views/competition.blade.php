@extends( 'layouts.base' )

@section( 'content' )

@parent

<script type="text/javascript">
	var protocol1_state = {!! json_encode( $competition->get_state()->to_array() ) !!};
	var protocol1_old_state = JSON.parse( JSON.stringify( protocol1_state ) );
</script>

@include( 'parts.save_button' )
@include( 'competition.index' )
<div class="competition_settings_group">
	@include( 'competition.settings_index' )
	@include( 'competition.settings_classes' )
	@include( 'competition.settings_handicaps' )
	@include( 'competition.settings_lanes' )
	@include( 'competition.settings_prices' )
	@include( 'competition.settings_series' )
</div>
@include( 'competition.registration' )
@include( 'competition.results' )
@include( 'competition.scoring' )
@include( 'competition.tools' )

<div class="back_button textcenter" hidden="hidden">
	<div class="back"><i class="fas fa-arrow-circle-left"></i> {{ __( 'Tillbaka' ) }}</div>
</div>

@endsection
