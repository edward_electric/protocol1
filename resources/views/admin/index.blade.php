@extends( 'admin.base' )

@section( 'h1', 'Admin' )

@section( 'content' )
	<div class="">
		<a href="{{ route( 'admin.settings' ) }}"><button class="button expand with_icon"><i class="fa fa-cog"></i></i> Inställningar</button></a>
	</div>
@append
