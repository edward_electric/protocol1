@extends( 'admin.base' )

@section( 'h1', 'Inställningar' )
@section( 'h1_icon', 'cog' )

@section( 'content' )
	@parent
	{!! $form !!}
@append
