@section( 'page_id', Route::currentRouteName() )
<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="/css/app.css?{{ @filemtime(public_path( '/css/app.css' )) }}" rel="stylesheet" type="text/css" />
        <title>@include( 'parts.title' )</title>
    </head>
    <body id="@yield('page_id')">

		@yield( 'header' )

		@if ( alerts()->count() > 0 )
			@include( 'parts.alerts.section' )
		@endif

    	<div class="row content">
    		<div class="small-12 columns">
    			@yield( 'content' )
    		</div>
    	</div>

  		@yield( 'footer' )

		<script type='text/javascript' src='/js/app.js?{{ @filemtime(public_path( '/js/app.js' )) }}'></script>
    </body>
</html>
