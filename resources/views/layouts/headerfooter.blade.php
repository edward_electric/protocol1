@extends( 'layouts.base' )

@section( 'header' )
	<h1><a href="">@yield( 'h1' )</a></h1>
@endsection

@section( 'footer' )
	@include( 'parts.footer' )
@endsection
