var protocol1 = new function()
{
	p1 = this;

	/**
		@brief		The ID of the current panel we are viewing.
		@since		2018-04-12 12:18:51
	**/
	p1.current_panel_id = '';

	/**
		@brief		Controls the timeout of the state watcher.
		@since		2018-04-08 18:12:44
	**/
	p1.state_watcher_timeout = false;

	/**
		@brief		Make a fieldset clickable collapsible.
		@since		2018-04-29 21:01:06
	**/
	p1.collapse_fieldset = function( $fieldsets )
	{
		var collapsed_html = '<i class="fa fa-arrow-right"></i>';
		var uncollapsed_html = '<i class="fa fa-arrow-down"></i>';
		$.each( $fieldsets, function( index, fieldset )
		{
			var $fieldset = $( fieldset );
			$fieldset.addClass( 'collapsible' );
			$fieldset.data( 'collapsed', false );
			var $legend = $( 'legend', $fieldset );
			$legend.data( 'original_label', $legend.html() );
			$legend.html( uncollapsed_html + ' ' + $legend.data( 'original_label' ) );
			$legend.click( function()
			{
				if ( $fieldset.data( 'collapsed' ) )
				{
					$fieldset.data( 'collapsed', false )
					$fieldset.children().not( 'legend' ).toggle();
					$legend.html( uncollapsed_html + ' ' + $legend.data( 'original_label' ) );
				}
				else
				{
					$fieldset.data( 'collapsed', true )
					$fieldset.children().not( 'legend' ).toggle();
					$legend.html( collapsed_html + ' ' + $legend.data( 'original_label' ) );
				}
			} );
			$legend.click();		// Collapse it to start off with.
		} )
	}

	/**
		@brief		Convenience method to count the keys in an object.
		@since		2018-04-11 17:28:49
	**/
	p1.count_object = function( object )
	{
		if ( jQuery.isArray( object ) )
			return object.length;
		return Object.keys( object ).length;
	}

	/**
		@brief		Disable all forms from being submitted.
		@since		2018-04-08 20:58:08
	**/
	p1.disable_forms = function()
	{
		$( 'form' ).submit( function( e )
		{
			e.preventDefault();
			return false;
		} );
	}

	/**
		@brief		Load a new state right now.
		@since		2019-04-28 21:33:58
	**/
	p1.fetch_state = function()
	{
		return $.ajax( {
			'data' : { 'main_hash' : protocol1_state.hashes.main },
			'dataType' : 'json',
			'type' : 'POST',
			'url' : protocol1_state.urls.get_state,
		} )
		.done( function( new_state )
		{
			p1.maybe_load_new_state( new_state );
		} );
	}

	/**
		@brief		Handle the forwards and backwards buttons and titles and all that.
		@since		2018-10-07 13:25:09
	**/
	p1.history =
	{
		/**
			@brief		Add a new panel to the history.
			@since		2018-10-07 13:25:56
		**/
		'add' : function( $target_panel )
		{
			if ( ! window.history )
				return;

			var id = $target_panel.attr( 'id' );
			var title = $target_panel.data( 'title' ) + ' | ' + protocol1_state.info.name;
			var url = document.location + '';	// Get just the string.
			var state = {
				'id' : id,
				'title' : title,
				'url' : url,
			};
			history.pushState( state, title, protocol1_state.url + '/' + id );
			document.title = title;
		},

		/**
			@brief		The HTML back button was pressed.
			@since		2018-10-07 14:36:27
		**/
		'back' : function()
		{
			history.back();
			p1.history.backed();
		},

		/**
			@brief		We have backed the history by one. Set title and url.
			@since		2018-10-07 13:26:02
		**/
		'backed' : function()
		{
			var state = history.state;
			if ( ! state )
			{
				// No history state left?
				p1.switch_panel( 'competition_index' );
				document.title = protocol1_state.info.name;
			}
			else
			{
				p1.switch_panel( state.id );
				document.title = state.title;
			}
		},

		/**
			@brief		Initialize the history.
			@since		2018-10-07 13:26:08
		**/
		'init' : function()
		{
			// History support needs to be enabled.
			if ( ! window.history )
				return;

			// Make the back button clickable.
			$( '.back_button' ).show()
				.click( function()
				{
					history.back();
				} );

			$(window).on('popstate', function()
			{
				p1.history.backed();
			});
		}
	};

	/**
		@brief		Common function to make all panels clickable.
		@since		2018-04-08 12:51:35
	**/
	p1.make_panels_clickable = function()
	{
		$( 'button' ).click( function()
		{
			var $button = $( this );
			var data = $button.data( 'switch_p1_panel' );
			if ( data == undefined )
				return;
			// Only allow active buttons to be pressed.
			if ( $button.hasClass( 'disabled' ) )
			{
				$button.blur();
				return;
			}
			var target_panel_id = $button.data( 'switch_p1_panel' );
			// Remember the latest panel we were on.
			var $current_panel = $button.parentsUntil( '.p1_panel' ).parent();
			p1.current_panel_id = $current_panel.attr( 'id' );

			var $target = p1.switch_panel( target_panel_id );
			p1.current_panel_id = target_panel_id;

			p1.history.add( $target );
		} );
	}

	/**
		@brief		Convenience method to maybe set this new_state as the current state.
		@return		true if the new_state actually was new.
		@since		2018-04-08 20:42:26
	**/
	p1.maybe_load_new_state = function( new_state )
	{
		// No difference?
		if ( new_state == protocol1_state.hashes.main )
			return false;

		protocol1_old_state = protocol1_state;
		protocol1_state = new_state;
		p1.load_state( new_state );

		return true;
	}

	/**
		@brief		Init.
		@since		2018-04-06 10:19:27
	**/
	p1.init = function()
	{
		// For our initial load, clear all of the hashes.
		$.each( protocol1_state.hashes, function( index, item )
		{
			protocol1_state.hashes[ index ] = Math.random();
		} );
		// And all inputs.
		$( 'input.text' ).val( '' );

		p1.load_state();
		p1.disable_forms();
		p1.history.init();
		p1.make_panels_clickable();
		p1.index.init();
		p1.scoring.init();
		p1.settings.init();
		p1.start_state_watcher();
		p1.registration.init();
		p1.tools.init();

// DEBUG
//		$( 'button.settings_index' ).click();
//		$( 'button.handicaps' ).click();
//		$( 'button.series' ).click();
//		$( 'button.scoring' ).click();
//		$( 'button.lanes' ).click();
//		$( 'button.registration' ).click();
//		$( 'button.tools' ).click();
//		$( '.timer legend' ).click();
	}

	/**
		@brief		Load the new state.
		@since		2018-04-08 02:43:47
	**/
	p1.load_state = function()
	{
		p1.index.load_state();
		p1.settings.load_state();
		p1.registration.load_state();
		p1.results.load_state();
		p1.scoring.load_state();
		p1.tools.load_state();
	}

	/**
		@brief		Convenience method to handle messages.
		@since		2018-04-13 06:37:00
	**/
	p1.message_box = function( $anchor )
	{
		var $message_box = $anchor;

		$message_box.$content = $( '.content', $message_box );

		/**
			@brief		Set and show the failure message.
			@since		2018-04-13 06:40:55
		**/
		$message_box.failure = function( message )
		{
			$message_box.reset();
			$message_box.$content.html( message )
			$message_box.addClass( 'failure' )
				.show();
		}

		/**
			@brief		Hide all messages.
			@since		2018-04-13 06:41:29
		**/
		$message_box.reset = function()
		{
			$message_box
				.removeClass( 'success' )
				.removeClass( 'failure' );
			$message_box.hide();
		}

		/**
			@brief		Set and show the success message.
			@since		2018-04-13 06:40:55
		**/
		$message_box.success = function( message )
		{
			$message_box.reset();
			$message_box.$content.html( message )
			$message_box.addClass( 'success' )
				.show();
		}

		return $message_box;
	}

	/**
		@brief		Convenience method to handle save buttons.
		@since		2018-04-08 22:10:24
	**/
	p1.save_button = function( $anchor )
	{
		var $button = $( 'div.save_button' ).clone().first();
		$button.removeClass( '.save_button' );

		$button.message_box = p1.message_box( $( '.message_box', $button ) );

		/**
			@brief		The callback after a successful update.
			@since		2018-04-08 23:21:09
		**/
		$button.done_callback = null;

		/**
			@brief		The callback for failed calls.
			@since		2018-04-29 21:21:44
		**/
		$button.fail_callback = null;

		/**
			@brief		Hide the save button upon successful update.
			@since		2018-04-08 22:42:41
		**/
		$button.hide_on_success = true;

		// Add some convenience methods.

		// Append to an element.
		$button.append_to = function( $element )
		{
			$button.appendTo( $element );
		}
		$button.append_to( $anchor );

		/**
			@brief		Is the button visible?
			@since		2018-05-20 15:05:45
		**/
		$button.is_visible = function()
		{
			return $button.is( ':visible' );
		}

		// Return the save button itself.
		$button.the_button = function()
		{
			return $( 'button', $button );
		}

		// Set the callback for fetching the data to be POSTed.
		$button.set_data_callback = function( callback )
		{
			$button[ 'data_callback' ] = callback;
		};

		// The callback for a successful update.
		$button.set_done_callback = function( callback )
		{
			$button[ 'done_callback' ] = callback;
		};

		// Set the callback for failure.
		$button.set_fail_callback = function( callback )
		{
			$button[ 'fail_callback' ] = callback;
		};

		// Set the URL to call to save the form.
		$button.set_url = function( url )
		{
			$button.url = url;
		};
		$button.set_url( protocol1_state.urls.set_state );

		$button.the_button().click( function()
		{
			$button.message_box.reset();

			var $this = $( this );

			// Don't do anything if disabled.
			if ( $this.hasClass( 'disabled' ) )
				return;

			$this.fadeTo( 100, 0.5 );
			var callback = $button[ 'data_callback' ];
			$.ajax( {
				'data' : callback(),
				'dataType' : 'json',
				'type' : 'post',
				'url' : $button.url,
			} )
			.fail( function( json )
			{
				$button.message_box.failure( 'Server error' );
				$this.fadeTo( 100, 1 );
				if ( $button.fail_callback !== null )
					$button.fail_callback( json );
			} )
			.done( function( json )
			{
				$this.fadeTo( 0, 1 );
				if ( p1.maybe_load_new_state( json ) )
					if ( $button.hide_on_success )
						$button.hide();
				if ( $button.done_callback !== null )
					$button.done_callback( json );
			} );
		} );

		return $button;
	}

	/**
		@brief		Handle an array of small and large inputs.
		@since		2018-04-09 23:09:44
	**/
	p1.small_and_large_inputs = function( options )
	{
		var smli = {};

		smli.options = jQuery.extend( {
			'$div' : null,					// Which div to work with. Contains the items and buttons we use.
			'save_key' : '',				// The json key used to save this data.
			'small_value_is_int' : true,	// Is the small value an int?
			'state' : null,					// A reference to the state that contains our working array.
		}, options );

		smli.$template = $( '.small_and_large_input_template', smli.options.$div );
		smli.$items = $( '.small_and_large_inputs', smli.options.$div );

		/**
			@brief		Add an item row from the template.
			@since		2018-04-09 23:19:25
		**/
		smli.add_item_row = function()
		{
			var $template = smli.$template.clone().first();
			$template.appendTo( smli.$items ).show();

			// Make the X button clickable.
			$( 'button.remove' ).click( function()
			{
				$( this ).parentsUntil( '.small_and_large_inputs' ).remove();
				smli.check_for_differences();
			} );

			// Focus on the new small value.
			$( '.input_large_value', $template ).focus();

			return $template;
		}

		smli.check_for_differences = function()
		{
			var difference = false;
			$smli_items = $( '.small_and_large_input', smli.$items );
			var count = smli.count_items();
			if ( $smli_items.length != count )
				difference = true;
			else
			{
				$.each( $smli_items, function( ignore, row )
				{
					var $row = $( row );

					var $small_input = $( '.input_small_value', $row );
					var small_value = $small_input.val();
					difference = difference || ( small_value != $small_input.attr( 'placeholder' ) );

					var $large_input = $( '.input_large_value', $row );
					var large_value = $large_input.val();
					difference = difference || ( large_value != $large_input.attr( 'placeholder' ) );
				} );
			}
			if ( difference )
				$save_button.show();
			else
				$save_button.hide();
		}

		/**
			@brief		Count the items in the state object.
			@since		2018-04-09 23:23:35
		**/
		smli.count_items = function()
		{
			return p1.count_object( smli.options.state );
		}

		/**
			@brief		Load this new state.
			@since		2018-04-09 23:35:01
		**/
		smli.load_state = function( new_state )
		{
			smli.options.state = new_state;
			smli.$items.empty();
			$.each( smli.options.state, function( large_value, small_value )
			{
				$item = smli.add_item_row();
				$( '.input_small_value', $item )
					.attr( 'placeholder', small_value )
					.blur()
					.val( small_value );
				$( '.input_large_value', $item )
					.attr( 'placeholder', large_value )
					.val( large_value );
			} );
			$( '.small_and_large_input input', smli.$items ).on( 'input', function()
			{
				smli.check_for_differences();
			} );
		}

		$( '.add_button_div button', smli.options.$div ).click( function()
		{
			smli.add_item_row();
			smli.check_for_differences();
		} );

		var $save_button = p1.save_button( $( '.save_button_div', smli.options.$div ) );

		$save_button.set_data_callback( function()
		{
			var data = {};

			$.each( $( '.small_and_large_input', smli.$items ), function( ignore, row )
			{
				var $row = $( row );

				var $small_input = $( '.input_small_value', $row );
				var small_value = $small_input.val();
				if ( small_value != '' )
				{
					if ( options.small_value_is_int )
						small_value = parseInt( $small_input.val() ) || 0;		// To get 0 as the lowest number.
				}
				else
					small_value = $small_input.attr( 'placeholder' );
				// Still no proper value? Assume 0.
				if ( ! small_value )
					small_value = '';

				var $large_input = $( '.input_large_value', $row );
				var large_value = $large_input.val();
				if ( large_value == '' )
					large_value = $large_input.attr( 'placeholder' );

				// Ignore empty values.
				if ( large_value == '' )
					return;

				data[ large_value ] = small_value;
			} );

			var r = {};
			r[ options.save_key ] = data;
			return r;
		} );

		// The default is to reload with the new state.
		$save_button.set_done_callback( function( new_state )
		{
			p1.maybe_load_new_state( new_state );
		} );

		smli.load_state( smli.options.state );

		return smli;
	}

	/**
		@brief
		@since		2018-04-08 17:49:20
	**/
	p1.start_state_watcher = function()
	{
		clearTimeout( p1.state_watcher_timeout );
		p1.state_watcher_timeout = setTimeout( function()
		{
			// Retrieve the current state.
			p1.fetch_state()
			.always( function()
			{
				// Schedule a new state watcher.
				p1.start_state_watcher();
			} );
		}, 1000 * 15 );		// Refresh every 15 seconds.
	}

	/**
		@brief		Show this panel.
		@since		2018-04-08 13:15:54
	**/
	p1.switch_panel = function( panel_id )
	{
		// Hide all panels.
		$( '.p1_panel' ).hide();
		// Show only the target.
		var $panel = $( '#' + panel_id );
		$panel.show();
		return $panel;
	}

	/**
		@brief		Convenience method to watch a form for changes and send the new values to the server.
		@since		2018-04-24 17:41:05
	**/
	p1.watch_save_form = function( options )
	{
		var wsf = {};

		wsf.options = jQuery.extend( {
			'$form' : null,				// Which input to watch.
			'data_callback' : null,			// The callback for fetching / generating the data to be saved.
		}, options );

		wsf.$inputs = $( 'input', wsf.options.$form );

		/**
			@brief		Has this input changed?
			@since		2018-04-24 19:43:34
		**/
		wsf.check_for_differences = function( $input )
		{
			if ( wsf.$save_button.hasClass( 'disabled' ) )
				return false;

			var handled = false;

			var val = $input.val();
			if ( val != $input.attr( 'placeholder' ) )
				return true;

			return false;
		}

		wsf.$save_button = p1.save_button( wsf.options.$form );

		wsf.$save_button.set_data_callback( wsf.options.data_callback );

		// The default is to reload with the new state.
		wsf.$save_button.set_done_callback( function( new_state )
		{
			p1.maybe_load_new_state( new_state );
		} );

		wsf.$inputs.on( 'input', function()
		{
			var difference = false;
			$.each( wsf.$inputs, function( index, input )
			{
				difference |= wsf.check_for_differences( $( input ) );
			} );
			if ( difference )
				wsf.$save_button.show();
			else
				wsf.$save_button.hide();

		} );

		return wsf;
	}

	/**
		@brief		Convenience method to watch a specific input for changes and then send the new value to the server.
		@since		2018-04-10 17:30:45
	**/
	p1.watch_save_input = function( options )
	{
		var wsi = {};

		wsi.options = jQuery.extend( {
			'$input' : null,				// Which input to watch.
			'data_callback' : null,			// The callback for fetching / generating the data to be saved.
			'save_key' : '',				// The json key used to save this data.
		}, options );

		wsi.check_for_differences = function()
		{
			if ( $save_button.hasClass( 'disabled' ) )
				return $save_button.hide();
			var difference = false;
			var val = wsi.options.$input.val();
			if ( val != wsi.options.$input.attr( 'placeholder' ) )
				difference = true;
			if ( val == '' )
				difference = false;
			if ( difference )
				$save_button.show();
			else
				$save_button.hide();
		}

		// Watch for changes.
		wsi.options.$input.on( 'input', function()
		{
			wsi.check_for_differences();
		} );

		var $save_button = p1.save_button( wsi.options.$input.parentsUntil( 'form' ).parent().parent() );

		// Assume a default callback.
		if ( wsi.options.data_callback === null )
			wsi.options.data_callback = function()
			{
				r = {};
				r[ options.save_key ] = wsi.options.$input.val();
				return r;
			};
		$save_button.set_data_callback( wsi.options.data_callback );

		// The default is to reload with the new state.
		$save_button.set_done_callback( function( new_state )
		{
			p1.maybe_load_new_state( new_state );
		} );

		return wsi;
	}
