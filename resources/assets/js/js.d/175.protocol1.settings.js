/**
	@brief		Handle the competition settings.
	@since		2018-04-08 14:07:29
**/
p1.settings = new function()
{
	var settings = this;

	var $$ = $( '#competition_settings_index' );

	// Main settings form stuff
	var $settings_form = $( 'form#settings', $$ );
	var $competition_name_input = $( '.input_competition_name', $settings_form );
	var $competition_section_input = $( '.input_competition_section', $settings_form );

	// Classes
	var classes_smli = null;

	// Handicaps
	var handicaps = {};
	var $handicaps_form = $( 'form#handicaps' );

	// Lanes
	var $lanes = $( '#competition_settings_lanes' );
	$lanes.form = $( 'form#lanes' );
	$lanes.$count = $( '.input_lanes_count', $lanes.form );
	$lanes.$individual = $( '.individual_lanes', $lanes );
	$lanes.$save_button = null;
	$lanes.save_button_div = $( '.save_button_div', $lanes );

	// Prices
	var prices_smli = null;

	// Are all of the settings free to be modified?
	var settings_locked = true;

	settings.series = {};
	settings.series.$div = $( '#competition_settings_series' );
	settings.series.ready = false;
	settings.series.$template = $( '.existing_series_template', settings.series.$div );
	settings.series.$save_button = null;
	settings.series.$next_lane = $( 'form#settings_series .form_item_next_lane input' );
	settings.series.$shots = $( 'form#settings_series .form_item_shots input' );
	settings.series.$time = $( 'form#settings_series .form_item_time input' );
	settings.series.$current = $( '.current.series', settings.series.$div );

	/**
		@brief		Build the lanes inputs and selectors.
		@since		2018-04-18 09:41:04
	**/
	settings.build_lanes = function()
	{
		var lane_count = $lanes.$count.val();
		var current_lanes = $( '.lane', $lanes.$individual ).length;

		// Remove excess lanes.
		while( current_lanes > lane_count )
		{
			$( '.lane', $lanes.$individual ).last().remove();
			current_lanes--;
		}

		// Add missing lanes.
		while( current_lanes < lane_count )
		{
			current_lanes++;
			var $template = $( '.individual_lanes_template', $lanes ).clone().children();
			$template.data( 'lane_id', current_lanes );
			$template.html( current_lanes );

			if ( protocol1_state.lanes.disabled[ current_lanes ] !== undefined )
				$template.addClass( 'disabled' );
			else
				$template.addClass( 'enabled' );

			// Click to enable and disable.
			$template.click( function()
			{
				var $item = $( this );
				if ( $item.hasClass( 'enabled' ) )
					$item
						.removeClass( 'enabled' )
						.addClass( 'disabled' );
				else
					$item
						.addClass( 'enabled' )
						.removeClass( 'disabled' );
				$lanes.$save_button.show();
			} );

			$template.appendTo( $lanes.$individual );
		}

		$( '.lane', $lanes.$individual ).removeClass( 'end' );
		$( '.lane', $lanes.$individual ).last().addClass( 'end' );
	}

	/**
		@brief		Init.
		@since		2018-04-08 02:44:15
	**/
	settings.init = function()
	{
		settings.init_handicaps();
		settings.init_classes();
		settings.init_lanes();
		settings.init_prices();
		settings.init_series();
		settings.init_competition_general();
		settings.maybe_lock();
	}

	/**
		@brief		Initialize the class handling.
		@since		2018-04-09 20:32:06
	**/
	settings.init_classes = function()
	{
		classes_smli = p1.small_and_large_inputs( {
			'$div' : $( '#competition_settings_classes' ),
			'small_value_is_int' : false,
			'state' : protocol1_state.classes,
			'save_key' : 'competition_classes',
		} );
		$( 'button.classes span.count' ).html( '(' + classes_smli.count_items() + ')' );
	}

	/**
		@brief		Init general competition settings.
		@since		2018-04-08 20:32:43
	**/
	settings.init_competition_general = function()
	{
		// Watch the whole form.
		p1.watch_save_form( {
			'$form' : $settings_form,
			'data_callback' : function()
			{
				return { 'info' :
					{
						'name' : $competition_name_input.val(),
						'section' : $competition_section_input.val(),
					}
				};
			},
		} );

		// The quicksettings we have to put in p1 in order for the save button to find them in the scope.
		p1.settings.$quicksettings_form = $( '#competition_quicksettings' );
		p1.settings.$quickselect = $( '.form_item_quicksetting select', p1.settings.$quicksettings_form );
		p1.settings.$quickselect.val( '' );
		p1.settings.$quickselect.change( function()
		{
			var value = p1.settings.$quickselect.val();
			if ( value == '' )
				return;
			p1.settings.$quickselect.val( '' );	// Select nothing.
			$.ajax( {
				'data' : {
					'load_quicksettings' : value,
				},
				'dataType' : 'json',
				'type' : 'post',
				'url' : protocol1_state.urls.set_state,
			} )
			.done( function()
			{
				p1.fetch_state();
			} );
		} );
	}

	/**
		@brief		Init handicaps.
		@since		2018-04-08 22:17:22
	**/
	settings.init_handicaps = function()
	{
		var $save_button = p1.save_button( $handicaps_form );

		handicaps.load_from_state = function()
		{
			$.each( protocol1_state.handicaps.handicaps, function( weapon_type, handicap )
			{
				var $input = $( '.input_handicap_' + weapon_type );
				$input.val( handicap );
			} );
			var $input = $( '.input_personal_handicaps' );
			$input.prop( 'checked', protocol1_state.handicaps.personal_handicaps == true );
		}
		handicaps.load_from_state();

		$save_button.set_data_callback( function()
		{
			var r = {};
			$.each( protocol1_state.handicaps.handicaps, function( weapon_type, handicap )
			{
				var $input = $( '.input_handicap_' + weapon_type );
				var value = $input.val();
				if ( value == '' )
					value = $input.attr( 'placeholder' );
				r[ weapon_type ] = value;
			} );
			var personal_handicaps = $( 'input.input_personal_handicaps', $handicaps_form ).prop( 'checked' );
			return { 'competition_handicaps' : r, 'personal_handicaps' : personal_handicaps };
		} );

		$save_button.set_done_callback( function()
		{
			handicaps.load_from_state();
		} );

		// Watch for changes.
		$( 'input.number, input.input_personal_handicaps', $handicaps_form ).change( function()
		{
			var difference = false;
			// Check for differences.
			$.each( protocol1_state.handicaps.handicaps, function( weapon_type, handicap )
			{
				var $input = $( '.input_handicap_' + weapon_type );
				var value = $input.val();
				if ( value == '' )
					return;
				difference = difference || ( value != $input.attr( 'placeholder' ) );
			} );

			var personal_handicaps = $( '.input_personal_handicaps', $handicaps_form ).prop( 'checked' ) ? true : false;
			if ( personal_handicaps != protocol1_state.handicaps.personal_handicaps )
				difference = true;

			if ( difference )
				$save_button.show();
			else
				$save_button.hide();
		} );
	}

	/**
		@brief		Lanes panel.
		@since		2018-04-10 18:32:17
	**/
	settings.init_lanes = function()
	{
		$lanes.$save_button = p1.save_button( $lanes.save_button_div );

		$lanes.$save_button.set_data_callback( function()
		{
			var lanes_available = {};
			var lanes_disabled = {};
			$.each( $( '.lane', $lanes.$individual ), function( ignore, lane )
			{
				var $lane = $( lane );
				var lane_id = $lane.data( 'lane_id' );
				if ( $lane.hasClass( 'disabled' ) )
					lanes_disabled[ lane_id ] = lane_id;
				else
					lanes_available[ lane_id ] = lane_id;
			} );
			return {
				'competition_lanes_available' : lanes_available,
				'competition_lanes_count' : $lanes.$count.val(),
				'competition_lanes_disabled' : lanes_disabled,
			};
		} );

		$lanes.$count.change( function()
		{
			settings.build_lanes();
			$lanes.$save_button.show();
		} );
	}

	/**
		@brief		Initialize the price handling.
		@since		2018-04-09 20:32:06
	**/
	settings.init_prices = function()
	{
		prices_smli = p1.small_and_large_inputs( {
			'$div' : $( '#competition_settings_prices' ),
			'state' : protocol1_state.prices,
			'save_key' : 'competition_prices',
		} );
		$( 'button.prices span.count' ).html( '(' + prices_smli.count_items() + ')' );
	}

	settings.init_series = function()
	{
		settings.series.build();

		// Make the add button work.
		$( '.add_new_series button', settings.series.$div ).click( function()
		{
			var $button = $( this );
			settings.series.add_row( {
				'type' : $button.data( 'type' ),
				'next_lane' : settings.series.$next_lane.prop( 'checked' ) ? 1 : 0,
				'shots' : settings.series.$shots.val(),
				'time' : settings.series.$time.val(),
			} );
		} );

		// Add a save button.
		settings.series.$save_button = p1.save_button( $( '.add_new_series', settings.series.$div ) );

		settings.series.$save_button.set_data_callback( function()
		{
			var r = [];
			$.each( $( '.row', settings.series.$current ), function( index, row )
			{
				var $row = $( row );
				var type = $( '.series.type', $row ).data( 'type' );
				var data = { 'type' : type };

				data[ 'next_lane' ] = $( '.series.type', $row ).attr( 'next_lane' );

				var time = $( '.series.type', $row ).attr( 'time' );
				time = parseInt( time );
				if ( time > 0 )
					data[ 'time' ] = time;
				data[ 'shots' ] = $( '.series.type', $row ).attr( 'shots' );

				r[ index ] = data;
			} );
			return { 'competition_series' : r };
		} );

		settings.series.ready = true;
	}

	/**
		@brief		Load the new state, if changed.
		@since		2018-04-08 22:17:29
	**/
	settings.load_state = function()
	{
		settings.maybe_lock();

		if ( protocol1_state.hashes.lanes != protocol1_old_state.hashes.lanes )
		{
			$( 'button.lanes span.count' ).html( '(' + protocol1_state.lanes.count + ')' );
			$lanes.$count
				.attr( 'placeholder', protocol1_state.lanes.count )
				.val( protocol1_state.lanes.count );
			settings.build_lanes();
		}

		if ( protocol1_state.hashes.series != protocol1_old_state.hashes.series )
		{
			$( 'button.series span.count' ).html( '(' + protocol1_state.series.series.length + ')' );
		}

		if ( protocol1_state.hashes.info != protocol1_old_state.hashes.info )
		{
			$competition_name_input.attr( 'placeholder', protocol1_state.info.name );
			document.title = protocol1_state.info.name;
			$competition_name_input.val( protocol1_state.info.name );
			$competition_section_input.attr( 'placeholder', protocol1_state.info.section );
			$competition_section_input.val( protocol1_state.info.section );
		}

		if ( protocol1_state.hashes.handicaps != protocol1_old_state.hashes.handicaps )
		{
			// Handicaps: Put the values as placeholders.
			var handicaps_set = 0;
			$.each( protocol1_state.handicaps.handicaps, function( weapon_type, handicap )
			{
				var $input = $( '.input_handicap_' + weapon_type );
				$input.attr( 'placeholder', handicap );
				if ( handicap > 0 )
					handicaps_set++;
			} );
			if ( protocol1_state.handicaps.personal_handicaps )
				handicaps_set++;
			$( 'button.handicaps span.count' ).html( '(' + handicaps_set + ')' );
			if ( handicaps.load_from_state !== undefined )
				handicaps.load_from_state();
		}

		if ( protocol1_state.hashes.prices != protocol1_old_state.hashes.prices )
		{
			// The first time we load is before we have had time to init.
			if ( prices_smli )
			{
				prices_smli.load_state( protocol1_state.prices );
				$( 'button.prices span.count' ).html( '(' + prices_smli.count_items() + ')' );
			}
		}

		if ( protocol1_state.hashes.classes != protocol1_old_state.hashes.classes )
		{
			// The first time we load is before we have had time to init.
			if ( classes_smli )
			{
				classes_smli.load_state( protocol1_state.classes );
				$( 'button.classes span.count' ).html( '(' + classes_smli.count_items() + ')' );
			}
		}

		if ( settings.series.ready )
			if ( ! settings.series.$save_button.is_visible() )
				settings.series.build();
	}

	/**
		@brief		Disable the save buttons when the competition has started.
		@since		2018-04-12 13:03:40
	**/
	settings.maybe_lock = function()
	{
		if ( ! settings_locked )
			return;

		// classes_smli is only valid then things are initialized.
		if ( classes_smli )
			if ( p1.count_object( protocol1_state.participants ) > 0 )
			{
				settings_locked = false;
				$( '.competition_settings_group .lockable_settings .save_button button' ).addClass( 'disabled' ).hide();
			}
	}

	/**
		@brief		Add a series type row from the template.
		@since		2018-04-10 21:22:31
	**/
	settings.series.add_row = function( options )
	{
		options = jQuery.extend( {
			'next_lane' : 1,
			'type' : 'precision',
			'shots' : 5,
			'time' : 0,
		}, options );

		options.time = parseInt( options.time );

		type = options.type;

		var $row = settings.series.$template.clone().first();
		$row.removeClass( 'existing_series_template' );

		$( '.series.type:not(.' + type + ')', $row ).remove();

		$row.appendTo( settings.series.$current ).show();

		$( '.series.type', $row ).attr( 'shots', options.shots );
		$( '.extra_info', $row ).append( ", " + options.shots + " skott" );

		if ( options.time > 0 )
		{
			// Save the attribute for the easy saving.
			$( '.series.type', $row ).attr( 'time', options.time );
			$( '.extra_info', $row ).append( ", " + options.time + "s" );
		}

		$( '.series.type', $row ).attr( 'next_lane', options.next_lane );
		if ( options.next_lane == 1 )
			$( '.extra_info', $row ).append( ", nästa bana" );
		else
			$( '.extra_info', $row ).append( ", samma bana" );

		$( '.remove_button', $row ).click( function()
		{
			$row.remove();
			settings.series.renumber();
		} );
		settings.series.renumber();
	}

	/**
		@brief		Build the series html from the state.
		@since		2018-04-10 21:11:21
	**/
	settings.series.build = function()
	{
		// Remove the existing series.
		settings.series.$current.empty();

		// And for each series in the settings, add a row.
		$.each( protocol1_state.series.series, function( index, series )
		{
			settings.series.add_row( {
				'next_lane' : series.next_lane,
				'shots' : series.shots,
				'type' : series.type,
				'time' : series.time
			} );
		} );
		settings.series.renumber();
	}

	/**
		@brief		Check the series for differences from the state.
		@since		2018-04-10 21:36:51
	**/
	settings.series.check_for_differences = function()
	{
		if ( ! settings.series.ready )
			return;

		var state_value = '';
		var html_value = '';

		$.each( protocol1_state.series.series, function( index, series )
		{
			state_value += series.type;
		} );

		$.each( $( '.series.type', settings.series.$current ), function( index, series )
		{
			html_value += $( series ).data( 'type' );
		} );

		if ( state_value == html_value )
			settings.series.$save_button.hide();
		else
			settings.series.$save_button.show();
	}

	/**
		@brief		Renumber the existing series.
		@since		2018-04-10 21:08:02
	**/
	settings.series.renumber = function()
	{
		var counter = 1;
		$.each( $( '.row', settings.series.$current ), function( index, series )
		{
			var $series = $( series );
			var value = '';
			$( '.series.number', $series ).html( counter );
			counter++;
		} );
		settings.series.check_for_differences();
	}
}
