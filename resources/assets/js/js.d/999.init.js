jQuery( document ).ready( function()
{
	function csrf_tokenize()
	{
		// Add the csrf token to all forms.
		var $csrf_token = $('meta[name="csrf-token"]');
		var csrf_token = $csrf_token.attr( 'content' );
		$( 'form' ).append( '<input type="hidden" name="_token" value="' + csrf_token + '" />' );
		$.ajaxSetup(
		{
			headers:
			{
				'X-CSRF-TOKEN': csrf_token,
			}
		} );
	}
	csrf_tokenize();
} );
