p1.tools = new function()
{
	var tools = this;

	var $$ = $( '#competition_tools' );

	var $send_email = $( '.send_email', $$ );
	$send_email.message_box = p1.message_box( $( '.message_box', $send_email ) );
	$send_email.button = $( '.input_email_send', $send_email )
	$send_to_www = $( '.input_send_to_www', $send_email )

	tools.passwords = {};
	tools.passwords.$div = $( '.passwords', $$ );
	tools.passwords.read = {};
	tools.passwords.read.$div = $( '.form_item_read_password' );
	tools.passwords.read.$input = $( '.checkbox.input_read_password', tools.passwords.read.$div );
	tools.passwords.read.$div.$password = $( '.description', tools.passwords.read.$div );
	tools.passwords.write = {};
	tools.passwords.write.$div = $( '.form_item_write_password' );
	tools.passwords.write.$input = $( '.checkbox.input_write_password', tools.passwords.write.$div );
	tools.passwords.write.$div.$password = $( '.description', tools.passwords.write.$div );

	tools.init = function()
	{
		tools.init_email_send();
		tools.init_passwords();
		tools.init_preloaded_participants();
		tools.init_timer();
		p1.collapse_fieldset( $( 'fieldset' ), $$ );
	}

	/**
		@brief		Handle sending of the e-mails.
		@since		2018-04-13 07:19:16
	**/
	tools.init_email_send = function()
	{
		$send_email.button.click( function()
		{
			$send_email.button.fadeTo( 100, 0.5 );
			$.ajax( {
				'data' : {
					'message' : $( '.input_message', $$ ).val(),
					'addresses' : $( '.input_email_recipients', $$ ).val(),
				},
				'dataType' : 'json',
				'type' : 'post',
				'url' : protocol1_state.urls.send_email,
			} )
			.always( function()
			{
				$send_email.button.fadeTo( 100, 1 );
			} )
			.fail( function( response )
			{
				var json = response.responseJSON;
				$send_email.message_box.failure( json.message );
			} )
			.done( function( json )
			{
				$send_email.message_box.success( json.message );
			} );
		} );

		$send_to_www.click( function()
		{
			$send_to_www.fadeTo( 100, 0.5 )
			$.ajax( {
				'dataType' : 'json',
				'type' : 'post',
				'url' : protocol1_state.urls.send_to_www,
			} )
			.always( function()
			{
				$send_to_www.fadeTo( 100, 1 )
			} )
			.fail( function( response )
			{
				console.log( response.responseJSON );
				var json = response.responseJSON;
				$send_email.message_box.failure( json.message );
			} )
			.done( function( json )
			{
				$send_email.message_box.success( json.message );
			} );
		} );
	}

	/**
		@brief		Handle the passwords.
		@since		2018-07-26 17:11:01
	**/
	tools.init_passwords = function()
	{
		// Watch the passwords form
		p1.watch_save_form( {
			'$form' : $( 'form', tools.passwords.$div ),
			'data_callback' : function()
			{
				var read_password = '';	// Assume no password.
				if ( tools.passwords.read.$input.prop( 'checked' ) )
					// Only set the read if there isn't already one set.
					if ( protocol1_state.info.read_password == '' )
					{
						read_password = Math.random() * new Date().getTime().toString().substr( -5 );		// Generate a random password.
						read_password = Math.round( read_password );
					}
					else
						read_password = protocol1_state.info.read_password;

				var write_password = '';	// Assume no password.
				if ( tools.passwords.write.$input.prop( 'checked' ) )
					// Only set the write if there isn't alwritey one set.
					if ( protocol1_state.info.write_password == '' )
					{
						write_password = Math.random() * new Date().getTime().toString().substr( -5 );		// Generate a random password.
						write_password = Math.round( write_password );
					}
					else
						write_password = protocol1_state.info.write_password;

				return { 'info' :
					{
						'read_password' : read_password,
						'write_password' : write_password,
					}
				};
			},
		} );
	}

	/**
		@brief		Load the preloaded participants.
		@since		2019-04-28 15:10:12
	**/
	tools.init_preloaded_participants = function()
	{
		tools.$preloaded_participants = $( '.form_item_preloaded_participants select' );
		if ( tools.$preloaded_participants.length < 1 )
			return;
		tools.$preloaded_participants.change( function()
		{
			var $input = $( this );
			var group_id = $input.val();
			$input.val( '' );

			$.ajax( {
				'data' : {
					'load_preloaded_participants' : group_id,
				},
				'dataType' : 'json',
				'type' : 'post',
				'url' : protocol1_state.urls.set_state,
			} )
			.done( function()
			{
				p1.fetch_state();
			} );
		} );
	}

	/**
		@brief		init_timer
		@since		2018-09-07 21:44:26
	**/
	tools.init_timer = function()
	{
		tools.timer.$timer = $( '.timer', $$ );

		tools.timer.$active_panel = $( '.active', tools.timer.$timer );
		tools.timer.$active_panel.$bar		= $( '.bar', tools.timer.$active_panel );
		tools.timer.$active_panel.$message	= $( '.message', tools.timer.$active_panel );

		tools.timer.$setup_panel = $( '.setup', tools.timer.$timer );

		// Make the buttons clickable.
		var $buttons = $( '.time.button', tools.timer.$setup_panel );
		$buttons.click( function()
		{
			// Unselect all buttons!
			$buttons.removeClass( 'selected' );
			$buttons.addClass( 'unselected' );
			var $button = $( this );
			$button.addClass( 'selected' );
			$button.removeClass( 'unselected' );
			console.log( $button.data( 'time' ) );
		} );

		$( '.start.button', tools.timer.$timer ).click( function()
		{
			// Get the time.
			var time = $( '.selected', tools.timer.$setup_panel ).data( 'time' );

			tools.timer.start( time );
		} );

		// It's not active. Hide the active panel.
		tools.timer.$active_panel.hide();
	}

	tools.load_state = function()
	{
		if ( protocol1_state.hashes.info != protocol1_old_state.hashes.info )
		{
			if ( protocol1_state.info.read_password == '' )
			{
				tools.passwords.read.$div.$password.hide();
				tools.passwords.read.$input.prop( 'checked', false );
			}
			else
			{
				tools.passwords.read.$div.$password.show();
				tools.passwords.read.$div.$password.html( 'Lösenordet är ' + protocol1_state.info.read_password );
				tools.passwords.read.$input.prop( 'checked', true );
			}

			// Clicked read automatically clicks write.
			tools.passwords.read.$input.change( function()
			{
				if ( tools.passwords.read.$input.prop( 'checked' ) )
					tools.passwords.write.$input.prop( 'checked', true )
			} );

			if ( protocol1_state.info.write_password == '' )
			{
				tools.passwords.write.$div.$password.hide();
				tools.passwords.write.$input.prop( 'checked', false );
			}
			else
			{
				tools.passwords.write.$div.$password.show();
				tools.passwords.write.$div.$password.html( 'Lösenordet är ' + protocol1_state.info.write_password );
				tools.passwords.write.$input.prop( 'checked', true );
			}
		}
	}

	/**
		@brief		Tool for helping count time.
		@since		2018-09-08 13:57:39
	**/
	tools.timer = {};
	tools.timer.prepare_section = function()
	{
		var section = tools.timer.sections[ tools.timer.section_index ];
		tools.timer.$active_panel.$message.html( section[ 'message' ] );
		tools.timer.section_timer_max = section[ 'time' ] * 1000;
		tools.timer.$active_panel.$bar.css( 'width', 0 );
		tools.timer.section_timer = 0;
	}
	tools.timer.start = function( time )
	{
		tools.timer.sections = [
			{ 'time' : 7,			'message' : '10 sekunder kvar' },
			{ 'time' : 3,			'message' : 'Färdiga' },
			{ 'time' : time - 3,	'message' : 'Eld!' },
			{ 'time' : 3,			'message' : 'Eld upphör' },
		];

		tools.timer.section_index = 0;

		var section_timer_increment = 25;

		tools.timer.prepare_section();

		tools.timer.$setup_panel.hide();
		tools.timer.$active_panel.show();

		tools.timer.interval = setInterval( function()
		{
			tools.timer.section_timer += section_timer_increment;
			if ( tools.timer.section_timer > tools.timer.section_timer_max )
			{
				tools.timer.section_index++;
				// Are we out of sections?
				if ( tools.timer.section_index > tools.timer.sections.length - 1 )
				{
					clearInterval( tools.timer.interval );
					tools.timer.$setup_panel.show();
					tools.timer.$active_panel.hide();
				}
				else
					tools.timer.prepare_section();
			}
			var width = tools.timer.section_timer / tools.timer.section_timer_max * 100;
			tools.timer.$active_panel.$bar.css( 'width', width + '%' );
		}, section_timer_increment );
	}
}
