p1.registration = new function()
{
	var registration = this;

	var $$ = $( '#competition_registration' );
	var $participant_class = $( '.form_item_participant_class select', $$ );
	var $participant_id = $( '.form_item_participant_id input', $$ );
	var $name = $( '.input_participant_name', $$ );
	var $lane = $( '.form_item_lane select', $$ );
	var $lane_quicklook = null;
	var $price = $( '.form_item_price select', $$ );
	var $save_button = null;
	var $weapon_group = $( '.form_item_weapon_group select', $$ );
	var $html_participants = $( '.html_participants', $$ );
	var $participant_quickselect = $( '.form_item_participant_quickselect select', $$ );
	var $personal_handicap = $( '.form_item_personal_handicap input', $$ );
	/**
		@brief		Convenience method to cound the options.
		@since		2018-04-11 11:32:09
	**/
	var count_options = function( $select )
	{
		return $( 'option', $select ).length;
	}

	/**
		@brief		Build the options in the select.
		@since		2018-04-11 11:24:56
	**/
	$lane.build = function()
	{
		var old_value = $lane.val();
		$( 'option', $lane ).remove();

		if ( ! $lane_quicklook )
		{
			$lane_quicklook = $( '<div>' );
			$lane_quicklook.addClass( 'lane_quicklook' );
			$lane_quicklook.appendTo( $( '.form_item_lane' ) );
		}
		$lane_quicklook.html( '' );

		var $option = $( '<option>' );
		$option.html( 'Välj bana' );
		$option.attr( 'value', '' );
		$option.appendTo( $lane );

		// Allow officials
		var $option = $( '<option>' );
		$option.html( 'Funktionär' );
		$option.attr( 'value', 'F' );
		$option.appendTo( $lane );

		// Allow practice
		var $option = $( '<option>' );
		$option.html( 'Träning' );
		$option.attr( 'value', 'T' );
		$option.appendTo( $lane );

		if ( $participant_id.val() > 0 )
		{
			var $option = $( '<option>' );
			$option.html( 'Ta bort' );
			$option.attr( 'value', 'delete' );
			$option.appendTo( $lane );
		}

		if ( p1.count_object( protocol1_state.lanes.free ) > 0 )
		{
			$.each( protocol1_state.lanes.available, function( ignore, lane_id )
			{
				var $option = $( '<option>' );
				var text = lane_id;

				// Show the participant name next to the lane number if the lane is busy.
				if ( protocol1_state.lanes.busy[ lane_id ] !== undefined )
				{
					text += " " + protocol1_state.lanes.busy[ lane_id ][ "participant_name" ];

					// If we are NOT editing a participant
					if ( $participant_id.val() < 1 )
						$option.attr( 'disabled', 'disabled' );
				}
				else
					$lane_quicklook.append( lane_id + ' ' );

				$option.html( text );
				$option.attr( 'value', lane_id );
				$option.appendTo( $lane );
			} );
		}
/**
		// Always allow to queue.
		var $option = $( '<option>' );
		$option.html( 'Kö' );
		$option.attr( 'value', 'queued' );
		$option.appendTo( $lane );
**/
		$lane.val( old_value );
	}

	/**
		@brief		Build the class options.
		@since		2018-04-11 11:30:39
	**/
	$participant_class.build = function()
	{
		$( 'option', $participant_class ).remove();
		$.each( protocol1_state.classes, function( description, index )
		{
			var $option = $( '<option>' );
			$option.html( description );
			$option.attr( 'value', index );
			if ( index == protocol1_state.registration_defaults.class )
				$option.attr( 'selected', 'selected' );
			$option.appendTo( $participant_class );
		} );
	}

	/**
		@brief		Build the class options.
		@since		2018-04-11 11:30:39
	**/
	$price.build = function()
	{
		$( 'option', $price ).remove();
		$.each( protocol1_state.prices, function( description, ignore )
		{
			var $option = $( '<option>' );
			$option.html( description );
			$option.attr( 'value', description );
			if ( description == protocol1_state.registration_defaults.price )
				$option.attr( 'selected', 'selected' );
			$option.appendTo( $price );
		} );
	}

	/**
		@brief		Build the weapon groups.
		@since		2018-04-11 11:30:39
	**/
	$weapon_group.build = function()
	{
		$( 'option', $weapon_group ).remove();
		$.each( protocol1_state.handicaps.handicaps, function( description, ignore )
		{
			var $option = $( '<option>' );
			$option.html( description );
			$option.attr( 'value', description );
			if ( description == protocol1_state.registration_defaults.weapon_group )
				$option.attr( 'selected', 'selected' );
			$option.appendTo( $weapon_group );
		} );
	}

	/**
		@brief		Check the inputs for changes worth being saved.
		@since		2018-04-11 16:06:33
	**/
	registration.check_for_changes = function()
	{
		var ready = true;
		var value = $name.val();
		value.trim();
		if ( value == '' )
			ready = false;
		if ( value.length < 3 )
			ready = false;

		if ( $participant_id.val() < 1 )
			// Check that this name isn't already taken.
			if ( protocol1_state.participants[ value ] !== undefined )
				ready = false;

 		if ( registration.get_select_value( $lane ) == '' )
			ready = false;

		if ( ready )
			$save_button.show();
		else
			$save_button.hide();
	}

	/**
		@brief		Return the selected option of a select input.
		@since		2018-04-11 16:29:28
	**/
	registration.get_select_value = function( $element )
	{
		var r = $element.find(":selected").attr( 'value' );
		if ( r === undefined )
			return '';
		return r;
	}

	/**
		@brief		Initialize
		@since		2018-04-11 11:05:52
	**/
	registration.init = function()
	{
		$name.on( 'input', function()
		{
			registration.check_for_changes();
		} );
		$lane.change( function()
		{
			registration.check_for_changes();
		} );
		$weapon_group.build();

		$save_button = p1.save_button( $( '.content form', $$ ) );
		$save_button.set_done_callback( function()
		{
			registration.reset();
			$lane.build();
			// Try to select the next lane.
			$lane.val( $save_button.old_lane_value + 1 );
			$lane.focus();
			$weapon_group.val( 'C' );
		} );
		$save_button.set_data_callback( function()
		{
			$save_button.old_lane_value = parseInt( $lane.val() );
			var data = {};
			data[ 'name' ] = $name.val();
			data[ 'lane' ] = registration.get_select_value( $lane );
			data[ 'price' ] = registration.get_select_value( $price );
			data[ 'participant_class' ] = registration.get_select_value( $participant_class );
			data[ 'participant_id' ] = $participant_id.val();
			data[ 'personal_handicap' ] = $personal_handicap.val();
			data[ 'weapon_group' ] = registration.get_select_value( $weapon_group );
			return { 'register_participant' : data };
		} );
		registration.check_for_changes();

		$lane.maybe_change_class = function()
		{
			var lane_id = $lane.val();
			var $option = $( 'option[value="' + lane_id + '"]', $participant_class );
			if ( $option.length < 1 )
				return;
			registration.set_select_value( $participant_class, lane_id );
		}

		// When the lane changes, also see if there's a related participant class.
		$lane.blur( function()
		{
			$lane.maybe_change_class();
		} );
		$lane.change( function()
		{
			$lane.maybe_change_class();
		} );

		if ( $participant_quickselect.length > 0 )
		{
			$participant_quickselect.change( function()
			{
				// Get the current value.
				var value = $participant_quickselect.val();
				if ( value == '' )
					return;
				// Find the option with this name.
				var $option = $( 'option[data-participant_name="' + value + '"]', $participant_quickselect );
				// Get all datas.
				$.each( $option.data(), function( data_name, data_value )
				{
					if ( data_value === '' )
						return;
					// Find the input that has this data_name.
					var key = '.form_item_' + data_name + ' select';
					key += ', .form_item_' + data_name + ' input';
					// If found, set the value.
					$( key, $$ ).val( data_value );
				} );
				// Select nothing!
				$participant_quickselect.val( '' ).blur();
				registration.check_for_changes();
			} );
			$participant_quickselect.val( '' );
		}

		registration.reset();
	}

	registration.load_state = function ()
	{
		if ( protocol1_state.hashes.lanes != protocol1_old_state.hashes.lanes )
			$lane.build();

		if ( count_options( $participant_class ) != protocol1_state.classes.length )
			$participant_class.build();

		if ( count_options( $price ) != protocol1_state.prices.length )
			$price.build();

		if ( protocol1_state.hashes.html != protocol1_old_state.hashes.html )
			$html_participants.html( protocol1_state.html.participants );

		if ( protocol1_state.handicaps.personal_handicaps )
			$( '.form_item_personal_handicap', $$ ).show();
		else
			$( '.form_item_personal_handicap', $$ ).hide();

		// Make the names editable.
		$( 'tr.editable td#name' ).click( function()
		{
			var $this = $( this );
			var participant_id = $this.parent().attr( 'participant_id' );
			var participant_name = $this.html();
			if ( ! confirm( 'Redigera ' + participant_name  + '?' ) )
				return;
			// For later lookups.
			var competitor = protocol1_state.participants[ participant_name ];
			$.ajax( {
				'data' : {
					'unregister_participant' : {
						'participant_id' : participant_id,
					},
				},
				'dataType' : 'json',
				'type' : 'post',
				'url' : protocol1_state.urls.set_state,
			} )
			.fail( function( json )
			{
				alert( 'Server error' );
			} )
			.done( function( json )
			{
				// Only assign a participant_id if the user has begun shooting.
				if ( competitor.scores > 0 )
					$participant_id.val( participant_id );
				else
					$participant_id.val( 0 );
				p1.maybe_load_new_state( json );
				$name.val( competitor.name );
				registration.set_select_value( $lane, competitor.lane );
				registration.set_select_value( $participant_class, competitor.participant_class );
				registration.set_select_value( $price, competitor.price_text );
				registration.set_select_value( $weapon_group, competitor.weapon_group );
				$lane.build();
				$save_button.show();
			} );
		} );
	}

	/**
		@brief		Reset the registration fields.
		@since		2018-06-12 11:01:19
	**/
	registration.reset = function()
	{
		$participant_id.val( 0 );
		$personal_handicap.val( 0 );
		$name.val( '' );
	}

	/**
		@brief		Set the value of a select input.
		@since		2018-04-15 22:51:58
	**/
	registration.set_select_value = function( $select, value )
	{
		$( $select ).val( value );
	}
}
