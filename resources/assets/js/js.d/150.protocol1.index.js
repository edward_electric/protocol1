p1.index = new function()
{
	var index = this;

	var $$ = $( '#competition_index' );
	var $button_registration = $( 'button.registration', $$ );
	var $button_results = $( 'button.results', $$ );
	var $button_scoring = $( 'button.scoring', $$ );

	/**
		@brief		Init.
		@since		2018-04-08 02:44:15
	**/
	index.init = function()
	{
	}

	index.load_state = function ()
	{
		$( '.competition_name', $$ ).html( protocol1_state.info.name );

		var lanes_busy = p1.count_object( protocol1_state.lanes.busy );
		if ( lanes_busy > 0 )
			$button_scoring.removeClass( 'disabled' );
		else
			$button_scoring.addClass( 'disabled' );

		if ( protocol1_state.results == '' )
			$button_results.addClass( 'disabled' );
		else
			$button_results.removeClass( 'disabled' );
	}
}
