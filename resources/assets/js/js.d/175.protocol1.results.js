p1.results = new function()
{
	var scoring = this;

	var $$ = $( '#competition_results' );
	var $detailed_scores_html = $( '.detailed_scores_html', $$ );
	var $scores_html = $( '.scores_html', $$ );
	var $money = $( '.money_div .money', $$ );

	scoring.load_state = function()
	{
		if ( protocol1_state.hashes.html == protocol1_old_state.hashes.html )
			return;
		$detailed_scores_html.html( protocol1_state.html.detailed_scores );
		$scores_html.html( protocol1_state.html.scores );
		$money.html( protocol1_state.money );
	}
}
