p1.scoring = new function()
{
	var scoring = this;

	var $$ = $( '#competition_scoring' );
	var $field_save_button = null;
	var $lane_and_participant = $( '.lane_and_participant', $$ );
	var $series_number = $( '.series_number', $$ );
	var $field_hits = $( '.field_score input.field_hits', $$ );
	var $field_save_and_finish = $( '.field_score .save_and_finish button', $$ );
	var $field_targets = $( '.field_score input.field_targets', $$ );
	var $precision_total = $( '.precision_score .precision_total', $$ );

	/**
		@brief		Finish scoring for this participant?
		@since		2018-04-12 16:52:27
	**/
	var finish_scoring = false;

	/**
		@brief		Does the user like using the total input, instead of the individual shots?
		@since		2019-05-05 14:25:51
	**/
	scoring.prefers_total = false;

	scoring.current_lane = '';					// The current lane.
	scoring.current_participant_name = '';		// The name of the current participant.
	scoring.current_series = '';				// The number of the current series.

	/**
		@brief		Is this shooter ready to save his last series?
		@since		2018-04-19 16:38:09
	**/
	scoring.almost_finished = function()
	{
		// Inputs are good.
		var name = scoring.current_participant_name;
		if ( ! name )
			return;
		var participant_scores = protocol1_state.participants[ name ].scores;
		var current_series = scoring.current_series;
		var max_series = protocol1_state[ 'series' ][ 'series' ].length;
		if ( current_series == max_series )
			if ( participant_scores >= max_series - 1 )
				return true;
		return false;
	}

	/**
		@brief		Find the first scorable user.
		@since		2018-04-11 20:43:51
	**/
	scoring.find_first = function()
	{
		if ( protocol1_state.lanes.busy.length < 1 )
			return;
		var first_key = Object.keys( protocol1_state.lanes.busy )[0];
		var first = protocol1_state.lanes.busy[ first_key ];
		var participant_name = first.participant_name;
		var first_unscored_series = scoring.first_unscored_series( participant_name );
		scoring.load_scores( participant_name, first_unscored_series );
	}

	/**
		@brief		Find the first available (unscored) series for this participant.
		@since		2018-04-11 20:51:06
	**/
	scoring.first_unscored_series = function( participant_name )
	{
		var participant = protocol1_state.participants[ participant_name ];

		// Find the max series number.
		var max = protocol1_state[ 'series' ][ 'series' ].length;

		// And find the first available for this
		for ( counter = 1; counter < max; counter++ )
		{
			var key = "score_" + counter;
			if ( participant[ key ] === undefined )
				break;
		}
		return counter;
	}

	/**
		@brief		Load the data for this new lane.
		@since		2018-04-11 21:50:22
	**/
	scoring.load_lane = function( lane_id )
	{
		var new_participant_name = protocol1_state.lanes.participants[ lane_id ];
		var first_unscored_series = scoring.first_unscored_series( new_participant_name );
		scoring.load_scores( new_participant_name, first_unscored_series );
	}

	/**
		@brief		Load and show the scores of a series of a participant.
		@since		2018-04-11 20:31:41
	**/
	scoring.load_scores = function( participant_name, series )
	{
		var series_data = protocol1_state[ 'series' ][ 'series' ][ series - 1 ];
		series_type = series_data.type;

		// Find the lane for this participant.
		var lane = protocol1_state.participants[ participant_name ][ 'lane' ];

		// Upon switching lanes, always disable the scoring.
		finish_scoring = false;

		// Set the participant name.
		$( '.lane', $lane_and_participant ).html( lane );
		$( '.participant_name', $lane_and_participant ).html( participant_name );
		scoring.current_participant_name = participant_name;
		scoring.current_lane = lane;

		// And set the series number.
		scoring.current_series = series;
		$( '.series_number', $series_number ).html( series );

		// Clear all values.
		$( '.series_type input', $$ ).val( '' );

		// Display the correct scoring fields.
		$( '.series_type', $$ ).hide();
		var $score = $( '.' + series_type + '_score', $$ ).show();

		if ( series_type == 'precision' )
		{
			// Kill all of the existing inputs.
			$(".single_shot:not(:first)", $score ).remove();
			$single_shot = $( '.single_shot', $score );
			for( counter = series_data.shots - 1 ; counter > 0 ; counter-- )
			{
				var $next_shot = $single_shot.clone();
				$( 'input', $next_shot ).removeClass( 'shot_1' )
					.removeClass( 'focus_me_first' )
					.addClass( 'shot_' + ( counter + 1 ) )
					.data( 'shot', counter + 1 )
					.attr( 'placeholder', '#' + ( counter + 1 ) );
				$next_shot.insertAfter( $single_shot );
			}

			// ----------
			//	PRECISION
			// ----------

			scoring.score_precision( {
				'$save_and_finish' : $( '.precision_score .save_and_finish', $$ ),
				'$save_button_container' : $( '.precision_score .save_button_container', $$ ),
				'$inputs' : $( '.precision_score input', $$ ),
				'$shots' : $( '.precision_score input.precision_shot', $$ ),
				'$total' : $( '.precision_score .precision_total', $$ ),
			} );

			$( '.precision_score input', $$ ).first().click( function()
			{
				scoring.prefers_total = false;
				console.log( 'prefers total', scoring.prefers_total );
			} );

			// Remember whether the user prefers to input using the total.
			$precision_total.click( function()
			{
				scoring.prefers_total = true;
				console.log( 'prefers total', scoring.prefers_total );
			} );

			$precision_total.attr( 'max', series_data.shots * 10 );

		}

		// Select the first input in the score type.
		if ( scoring.prefers_total )
			$( '.precision_score .precision_total', $$ ).focus();
		else
			$( 'input.focus_me_first', $score ).focus().change();

		// If there are scores, display them.
		var key = "score_" + series;
		var participant = protocol1_state.participants[ participant_name ];
		if ( participant[ key ] !== undefined )
		{
			if ( series_type == 'field' )
			{
				$field_hits.val( participant[ key ] );
				var key = "score_meta_" + series;
				$field_targets.val( participant[ key ][ 0 ] );
			}

			if ( series_type == 'precision' )
			{
				$precision_total.val( participant[ key ] );
				var key = "score_meta_" + series;
				if ( participant[ key ] !== undefined )
				{
					for( counter = 0 ; counter < series_data.shots ; counter++ )
						$( '.shot_' + ( counter + 1 ), $$ ).val( participant[ key ][ counter ] );
				}
			}
		}

		if ( series_type == 'field' )
			$field_hits.change();		// To force the save button to show up.
		if ( series_type == 'precision' )
			$precision_total.change();		// To force the save button to show up.
	}

	scoring.init = function()
	{
		// Upon opening the scoring panel, find the first participant that needs scoring.
		$( '#competition_index button.scoring' ).click( function()
		{
			scoring.find_first();
		} );

		$( '.next_lane', $$ ).click( function()
		{
			if ( scoring.is_finished() )
				return;
			var lane_id = scoring.current_lane;
			lane_id++;
			while ( lane_id <= protocol1_state.lanes.last_lane )
			{
				if ( protocol1_state.lanes.participants[ lane_id ] !== undefined )
					return scoring.load_lane( lane_id );
				lane_id++;
			}
			scoring.load_lane( protocol1_state.lanes.first_lane );
		} );

		$( '.next_series', $$ ).click( function()
		{
			if ( scoring.is_finished() )
				return;
			var max = protocol1_state[ 'series' ][ 'series' ].length;
			scoring.current_series++;
			if ( scoring.current_series > max )
				scoring.current_series = 1;
			scoring.load_scores( scoring.current_participant_name, scoring.current_series );
		} );

		$( '.previous_lane', $$ ).click( function()
		{
			if ( scoring.is_finished() )
				return;
			var lane_id = scoring.current_lane;
			lane_id--;
			while ( lane_id >= protocol1_state.lanes.first_lane )
			{
				if ( protocol1_state.lanes.participants[ lane_id ] !== undefined )
					return scoring.load_lane( lane_id );
				lane_id--;
			}
			scoring.load_lane( protocol1_state.lanes.last_lane );
		} );

		$( '.previous_series', $$ ).click( function()
		{
			if ( scoring.is_finished() )
				return;
			var max = protocol1_state[ 'series' ][ 'series' ].length;
			scoring.current_series--;
			if ( scoring.current_series < 1 )
				scoring.current_series = max;
			scoring.load_scores( scoring.current_participant_name, scoring.current_series );
		} )

		// ------
		//	FIELD
		// ------

		$field_save_and_finish.click( function()
		{
			$field_save_and_finish.fadeTo( 100, 0.5 );
			finish_scoring = true;
			$field_save_button.the_button().click();
		} );

		$field_save_button = p1.save_button( $( '.field_score .save_button_container', $$ ) );
		$field_save_button.hide_on_success = false;
		$field_save_button.set_data_callback( function()
		{
			var data = {};
			data[ 'participant_name' ] = scoring.current_participant_name;
			data[ 'series' ] = scoring.current_series;
			data[ 'score' ] = $field_hits.val();
			data[ 'score_meta' ] = [ $field_targets.val() ];
			if ( finish_scoring )
				data[ 'finish' ] = true;
			return { 'save_score' : data };
		} );
		$field_save_button.set_done_callback( function()
		{
			$field_save_and_finish.hide();
			$( '.next_lane', $$ ).click();
		} );
		$field_save_button.set_fail_callback( function()
		{
			$field_save_and_finish.fadeTo( 100, 1 );
		} );

		// Prevent the save button from being pressed if the inputs are crazy.
		$( '.field_score input', $$ ).change( function()
		{
			$field_save_and_finish.hide();
			$field_save_button.hide();

			var valid = true
				&& ( $field_hits.val() != '' )					// Must contain something.
				&& $field_hits[ 0 ].checkValidity()
				&& ( $field_targets.val() != '' )				// Must contain something.
				&& $field_targets[ 0 ].checkValidity();
			if ( valid )
			{
				if ( scoring.almost_finished() )
					$field_save_and_finish.show();
				else
					$field_save_button.show();
			}
		} ).first().change();
	}

	/**
		@brief		Return whether all scoring is done.
		@since		2018-04-12 10:16:10
	**/
	scoring.is_finished = function()
	{
		return ( Object.keys( protocol1_state.lanes.busy ).length < 1 );
	}

	scoring.load_state = function()
	{
		// If there are no more participants shooting, then quit.
		if ( p1.current_panel_id == 'competition_scoring' )
			if ( scoring.is_finished() )
				p1.history.back();
	}

	/**
		@brief		Common method for handling precision scoring.
		@since		2018-04-16 19:54:02
	**/
	scoring.score_precision = function( options )
	{
		var handler = {};
		handler.$save_and_finish = options.$save_and_finish;

		handler.check_inputs = function()
		{
			var empty_shots = 0;
			var has_total = ( options.$total.val() != '' );
			var pass = true;

			// Count how many shots are empty.
			$.each( options.$shots, function( index, item )
			{
				var $shot = $( item );
				pass &= $shot[ 0 ].checkValidity();
				if ( $shot.val() == '' )
				{
					empty_shots++;
					$shot.addClass( 'empty' );
					$shot.removeClass( 'not_empty' );
				}
				else
				{
					$shot.addClass( 'not_empty' );
					$shot.removeClass( 'empty' );
				}
			} );

			// All shots are empty.
			if ( empty_shots == options.$shots.length )
			{
				// But a total? Good!
				if ( has_total )
					pass &= options.$total[ 0 ].checkValidity();
				else
					pass = false;
			}
			else
			{
				if ( empty_shots == 0 )
				{
					// Each of the shots must be valid.
					$.each( options.$shots, function ( index, input )
					{
						var value = $( input ).val();
						if ( value == '' )
							return;
						if ( ! input.checkValidity() )
							pass = false;
					} );
				}

				// At least one empty shot.
				if ( empty_shots > 0 )
					pass = false;
			}
			// Check the total for validity.
			pass &= options.$total[ 0 ].checkValidity();

			return pass;
		}

		// Since this function is called each time a lane is changed, don't keep adding a click handler.
		handler.$save_and_finish.off( 'click' );
		handler.$save_and_finish.click( function()
		{
			handler.$save_and_finish.fadeTo( 100, 0.5 );
			finish_scoring = true;
			handler.$save_button.the_button().click();
		} )

		// Remove existing save buttons.
		$( options.$save_button_container ).children().remove();

		handler.$save_button = p1.save_button( options.$save_button_container );
		handler.$save_button.hide_on_success = false;
		handler.$save_button.set_data_callback( function()
		{
			var data = {};
			data[ 'participant_name' ] = scoring.current_participant_name;
			data[ 'series' ] = scoring.current_series;
			data[ 'score' ] = options.$total.val();
			data[ 'score_meta' ] = options.$shots.map(function(){return $(this).val();}).get();
			if ( finish_scoring )
				data[ 'finish' ] = true;
			return { 'save_score' : data };
		} );
		handler.$save_button.set_done_callback( function()
		{
			var series_type = protocol1_state[ 'series' ][ 'series' ][ scoring.current_series - 1 ];
			if ( series_type.next_lane == 0 )
				$( '.next_series', $$ ).click();
			else
				$( '.next_lane', $$ ).click();

			handler.$save_and_finish.fadeTo( 0, 1 ).hide();
			handler.$save_button.hide();
		} );

		handler.$save_button.set_fail_callback( function()
		{
			handler.$save_and_finish.fadeTo( 100, 1 );
		} );

		options.$inputs.off();
		options.$shots.off();
		options.$total.off();

		// Always recalculate when losing focus.
		options.$inputs.blur( function()
		{
			$( this ).trigger( 'input' );
		} );

		// Prevent the save button from being pressed if the inputs are crazy.
		options.$inputs.on( 'input', function()
		{
			handler.$save_and_finish.hide();
			handler.$save_button.hide();

			if ( handler.check_inputs() )
			{
				if ( scoring.almost_finished() )
					handler.$save_and_finish.show();
				else
					handler.$save_button.show();
			}
		} ).first().trigger( 'input' );

		// Autocalculate the precisions if no total is set.
		options.$inputs.on( 'input', function()
		{
			var $input = $( this );
			if ( $input.hasClass( 'precision_total' ) )
				return;
			var total = 0;
			var empty = false;
			$.each( options.$shots, function( ignore, shot )
			{
				var value = $( shot ).val();
				if ( value == '' )
				{
					empty = true;
					return;
				}
				value = parseInt( value );
				if ( value > -1 )
					total += value;
			} );
			if ( empty )
				return;
			if ( total < 1 )
				total = '';
			options.$total.val( total );
		} );

		/**
			@brief		Store previous value to check if empty before going backwards.
			@since		2019-01-02 18:20:40
		**/
		options.$total.on( 'keydown', function( event )
		{
			$( event.target ).data( 'previous_value', $( event.target ).val() );
		} );

		/**
			@brief		Go backwards from the total.
			@since		2019-01-02 18:20:40
		**/
		options.$total.on( 'keyup', function( event )
		{
			var key = event.keyCode || event.charCode;
			if ( key != 8 )
				return;

			var previous_value = $( event.target ).data( 'previous_value' );

			// It must be empty.
			if ( previous_value != '' )
				return;

			// Find the last shot.
			options.$shots.last().val( '' ).focus();
		} );

		/**
			@brief		Store previous value to check if empty before going backwards.
			@since		2019-01-02 18:20:40
		**/
		options.$shots.on( 'keydown', function( event )
		{
			$( event.target ).data( 'previous_value', $( event.target ).val() );
		} );

		/**
			@brief		Backspace = go to previous input
			@since		2019-01-02 17:01:50
		**/
		options.$shots.on( 'keyup', function( event )
		{
			var key = event.keyCode || event.charCode;
			if ( key != 8 )
				return;
			var $shot = $( this );

			// We can't go back from 1.
			if ( $shot.hasClass( 'shot_1' ) )
				return;

			var previous_value = $( event.target ).data( 'previous_value' );

			// It must be empty.
			if ( previous_value != '' )
				return;

			var shot = $shot.data( 'shot' );
			shot--;
			// Don't have the console open if you want to debug this.
			$( 'input.shot_' + shot ).val( '' ).focus();
		} );

		// Automove shots.
		options.$shots.on( 'input', function( event )
		{
			var key = event.keyCode || event.charCode;
			if ( key !== undefined )
				return;

			var $shot = $( this );
			var shot = $shot.val();
			if ( shot.trim() == '' )
				return;

			// Convert to int.
			shot = parseInt( shot );
			if ( shot == 1 )	// Because 1 could be 1 or 10
				return;

			var carry = 0;
			if ( shot > 10 )
			{
				$shot.val( 1 );
				carry = shot - 10;
			}

			// Find the next input to focus.
			var focus_found = false;
			$.each( options.$shots, function( index, options_shot )
			{
				var $options_shot = $( options_shot );
				if ( focus_found )
				{
					// Having inputted a > 10, the "remainder" should be inputted here, if empty.
					$options_shot.focus().select();
					if ( carry > 0 )
					{
						$options_shot.val( carry );
						carry = 0;
						// Leave the focus_found alone to go to the next input.
					}
					else
						focus_found = false;
					return;
				}

				if ( $options_shot.is( ':focus' ) )
				{
					focus_found = true;
				}
			} );

			// Was the last input focused?
			if (  focus_found )
				options.$total.focus();
		} );

		return handler;
	}
}
