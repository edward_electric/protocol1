<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get( '{api_key}/competition/{competition_key}', 'CompetitionController@get_api' );
Route::get( '{api_key}/competition/{competition_key}/get_state', 'CompetitionController@get_state_api' );
Route::get( '{api_key}/competition/{competition_key}/set_state', 'CompetitionController@set_state_api' );
Route::post( '{api_key}/competition/{competition_key}/set_state', 'CompetitionController@set_state_api' );
