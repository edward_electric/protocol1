<?php

Route::get( '/', 'IndexController@index' )->name( 'index' );
Route::post( '/', 'IndexController@index_post' );

Route::middleware( [ '\\App\\Http\\Middleware\\Competition_Read_Password' ] )->group( function()
{
	Route::get( '/competition/{competition_key}', 'CompetitionController@get' )												->name( 'competition.get' );
	Route::get( '/competition/{competition_key}/export/results/ods', 'CompetitionController@get_results_ods' )				->name( 'competition.export.results.ods' );
	Route::get( '/competition/{competition_key}/export/results/pdf', 'CompetitionController@get_results_pdf' )				->name( 'competition.export.results.pdf' );
	Route::get( '/competition/{competition_key}/export/results/txt', 'CompetitionController@get_results_txt' )				->name( 'competition.export.results.txt' );
	Route::get( '/competition/{competition_key}/export/results/xls', 'CompetitionController@get_results_xls' )				->name( 'competition.export.results.xls' );
	Route::get( '/competition/{competition_key}/export/participation/txt', 'CompetitionController@get_participants_txt' )	->name( 'competition.export.participants.txt' );
	Route::get( '/competition/{competition_key}/get_state', 'CompetitionController@get_state' );
	Route::post( '/competition/{competition_key}/get_state', 'CompetitionController@get_state' )							->name( 'competition.get_state' );
	Route::get( '/competition/{competition_key}/{page}', 'CompetitionController@redirect_to_competition' );
} );
Route::middleware( [ '\\App\\Http\\Middleware\\Competition_Write_Password' ] )->group( function()
{
	Route::post( '/competition/{competition_key}/send_email', 'CompetitionController@send_email' )							->name( 'competition.send_email' );
	Route::post( '/competition/{competition_key}/send_to_www', 'CompetitionController@send_to_www' )						->name( 'competition.send_to_www' );
	Route::post( '/competition/{competition_key}/set_state', 'CompetitionController@set_state' )							->name( 'competition.set_state' );
} );

Route::middleware( [ 'auth' ] )->prefix( 'admin' )->group( function()
{
	Route::get( '/', 'Admin\\Index@get' )																		->name( 'admin.index' );
	Route::get( '/settings', 'Admin\\Settings@get' )															->name( 'admin.settings' );
	Route::post( '/settings', 'Admin\\Settings@post' );
} );


Route::get( '/login', 'Auth\LoginController@get' )->name( 'login' );
Route::post( '/login', 'Auth\LoginController@post' );
Route::get( '/logout', 'Auth\LogoutController@get' )->name( 'logout' );
