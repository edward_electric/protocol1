<?php

namespace App\Http\Middleware;

use App\Models\Competition;
use App\Models\Meta;
use Closure;
use Exception;

/**
	@brief		Check for a valid API key.
	@since		2019-04-28 10:13:30
**/
class API_Key
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle( $request, Closure $next )
    {
		$competition_key = $request->route()->parameter( 'competition_key' );
		$api_key = $request->route()->parameter( 'api_key' );

    	try
    	{
			// Find the group with this meta.
			$meta = Meta::where( 'meta_key', 'p1_api_key' )
				->where( 'meta_value', $api_key )
				->firstOrFail();

			$group_id = $meta->object_id;

			// Create the competition
			$competition = Competition::maybe_create( $competition_key );
			// And assign the group.
			$competition->meta()->set( 'group', $group_id );

			return $next( $request );
    	}
    	catch( Exception $e )
    	{
			$alert = alerts()->alert();
			$alert->set_message( 'Incorrect API key' );
			return redirect()->route( 'index' );
    	}
    }
}
