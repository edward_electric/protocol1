<?php

namespace App\Http\Middleware;

use App\Models\Competition;

/**
	@brief		Check that the visitor knows the competition read password.
	@since		2018-07-26 20:27:52
**/
class Competition_Read_Password
	extends Competition_Password
{
	/**
		@brief		Return the password type for this check.
		@since		2018-07-26 20:31:22
	**/
	public function get_password( Competition $competition )
	{
		$r = [];
		$pw = $competition->meta()->get( 'read_password' );
		if ( $pw != '' )
			$r [] = $pw;
		$pw = $competition->meta()->get( 'write_password' );
		if ( $pw != '' )
			$r [] = $pw;
		return $r;
	}
}
