<?php

namespace App\Http\Middleware;

use App\Models\Competition;
use Closure;
use Exception;

/**
	@brief		Generic check for competition r/w passwords.
	@since		2018-07-26 20:27:52
**/
class Competition_Password
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle( $request, Closure $next )
    {
		$competition_key = $request->route()->parameter( 'competition_key' );
		$competition = Competition::maybe_create( $competition_key );

    	try
    	{
    		$passwords = $this->get_password( $competition );
    		// No password? Good to go!
    		if ( count( $passwords ) < 1 )
				return $next( $request );

			$session_key = $competition_key . '_password';
			$session_password = session( $session_key );
			if ( ! in_array( $session_password, $passwords ) )
				throw new Exception( 'Tävlingen behöver ett lösenord!' );

			return $next( $request );
    	}
    	catch( Exception $e )
    	{
			$alert = alerts()->alert();
			$alert->set_message( 'Tävlingen behöver lösenord!' );
			return redirect()->route( 'index' );
    	}
    }
}
