<?php

namespace App\Http\Middleware;

use App\Models\Competition;

/**
	@brief		Check that the visitor knows the competition write password.
	@since		2018-07-26 20:27:52
**/
class Competition_Write_Password
	extends Competition_Password
{
	/**
		@brief		Return the password type for this check.
		@since		2018-07-26 20:31:22
	**/
	public function get_password( Competition $competition )
	{
		// Only the write password is good enough for us.
		return [ $competition->meta()->get( 'write_password' ) ];
	}
}
