<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class LogoutController extends Controller
{
    /**
    	@brief		Do nothing but redirect the user.
    	@since		2018-05-27 16:49:57
    **/
    public function get()
    {
        Auth::logout();
        return redirect()->route( 'index' );
    }
}
