<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
    	@brief		The login window.
    	@since		2018-05-27 16:54:13
    **/
    public function get()
    {
    	$login_form = $this->get_login_form();

		return view( 'auth/login', [
			'login_form' => $login_form,
		] );
    }

    /**
    	@brief		Return the login form.
    	@since		2018-05-27 16:54:49
    **/
    public function get_login_form()
    {
		$form = $this->form();

		$form->email( 'email' )
			->label( 'Din e-postadress' );

		$form->password( 'password' )
			->label( 'Lösenord' );

		$form->submit( 'login' )
			->value( 'Logga in' );

		if ( $form->is_posting() )
		{
			$form->post();
			$form->use_post_values();
		}

		return $form;
    }

    /**
    	@brief		Post the login form.
    	@since		2018-05-27 16:58:54
    **/
    public function post()
    {
    	$login_form = $this->get_login_form();

    	$email = $login_form->input( 'email' )->get_filtered_post_value();
    	$password = $login_form->input( 'password' )->get_filtered_post_value();

		if ( Auth::attempt( ['email' => $email, 'password' => $password ] ) )
			return redirect()->route( 'index' );

		$alert = alerts()->alert();
		$alert->set_message( 'Fel e-postadress och / eller lösenord.' );

		return redirect()->back();
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
