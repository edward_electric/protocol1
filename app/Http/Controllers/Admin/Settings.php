<?php

namespace App\Http\Controllers\Admin;

use Exception;

/**
	@brief		Admin settings.
	@since		2019-01-08 19:28:37
**/
class Settings
	extends \App\Http\Controllers\Controller
{
	/**
		@brief		Return the form.
		@since		2019-01-08 19:29:16
	**/
	public function get_form()
	{
		$form = $this->form();
		$user = app()->user();

		$form->group_name = $form->text( 'group_name' )
			->label( 'Föreningens namn' )
			->required()
			->trim()
			->value( $user->group->name );

		$form->p1_api_key = $form->text( 'p1_api_key' )
			->description( 'API-nyckel för att kommunicera med hemsidan.' )
			->label( 'P1 API-nyckel' )
			->trim()
			->value( $user->group->meta()->get( 'p1_api_key' ) );

		$form->email_recipients = $form->textarea( 'email_recipients' )
			->label( 'Tävlingsresultaten mailas till angivna adresser. En adress per rad.' )
			->label( 'E-postmottagare' )
			->required()
			->rows( 4 )
			->value( $user->group->meta()->get( 'email_recipients' ) );

		$form->participant_quickselect = $form->textarea( 'participant_quickselect' )
			->label( 'Deltagarsnabbval' )
			->rows( 10, 40 );

		$participants = $user->group->meta()->get( 'participant_quickselect' );
		if ( $participants )
		{
			$participants = json_decode( $participants );
			$participant = reset( $participants );
			$text = implode( "\t", array_keys( (array) $participant ) ). "\n";
			foreach( $participants as $participant )
				$text .= implode( "\t", (array) $participant ) . "\n";
			$form->participant_quickselect->value( $text );
		}

		$form->submit( 'save' )
			->value( 'Spara' );

		if ( $form->is_posting() )
		{
			$form->post();
			$form->use_post_values();
		}

		return $form;
	}

	/**
		@brief		GET the page.
		@since		2019-01-08 19:28:45
	**/
	public function get()
	{
		return view( 'admin/settings', [
			'form' => $this->get_form(),
		] );
	}

	/**
		@brief		Handle the new participants.
		@since		2018-05-03 11:41:25
	**/
	public function post()
	{
		try
		{
			$form = $this->get_form();
			$user = app()->user();

			$group = $user->group;
			$group->name = $form->group_name->get_filtered_post_value();
			$group->save();

			$group->meta()->set( 'email_recipients', $form->email_recipients->get_filtered_post_value() );
			$group->meta()->set( 'p1_api_key', $form->p1_api_key->get_filtered_post_value() );

			// participant_quickselect
			// -----------------------
			$lines = $form->participant_quickselect->get_post_value();
			$lines = \plainview\sdk\base::textarea_to_array( $lines );

			if ( count( $lines ) > 0 )
			{
				$headers = array_shift( $lines );
				$headers = explode( "\t", $headers );

				// There should be at least two columns.
				if ( count( $headers ) < 2 )
					throw new Exception( 'Minst två kolumner måste anges!' );

				$participants = [];
				foreach( $lines as $line )
				{
					$columns = explode( "\t", $line );
					if ( count( $columns ) != count( $headers ) )
						throw new Exception( sprintf( 'Fel antal kolumner: %s', $line ) );
					$participant = (object)[];
					foreach( $headers as $header_index => $header )
						$participant->$header = $columns[ $header_index ];
					$participants []= $participant;
				}

				$user = app()->user();
				$participants = $user->group->meta()->set( 'participant_quickselect', $participants );
			}

			alerts()->success()->set_message( 'Sparat!' );

			return $this->get();
		}
		catch ( Exception $e )
		{
			alerts()->alert()->set_message( $e->getMessage() );
			return $this->get();
		}
	}
}
