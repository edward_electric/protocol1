<?php

namespace App\Http\Controllers\Admin;

/**
	@brief		Admin index.
	@since		2019-01-08 19:47:39
**/
class Index
	extends \App\Http\Controllers\Controller
{
	/**
		@brief		Admin index.
		@since		2018-05-03 10:54:50
	**/
	public function get()
	{
		return view( 'admin/index' );
	}
}
