<?php

namespace App\Http\Controllers;

use App\Models\Competition;

/**
	@brief		The main index.
	@since		2018-04-07 21:01:11
**/
class IndexController extends Controller
{
	/**
		@brief		Build the comp search / edit form.
		@since		2018-04-07 21:37:27
	**/
	public function get_form()
	{
		$form = $this->form();

		$form->text( 'competition_key' )
			->description( __( 'ID-texten är unik för varje tävling och en ny slumpas fram varje gång du laddar om denna sida.' ) )
			->label( __( 'TävlingsID' ) )
			->trim()
			->value( Competition::get_new_key() );

		$form->text( 'password' )
			->description( 'Om tävlingen är lösenordsskyddad, skriv in lösenordet här.' )
			->label( 'Lösenord' )
			->trim();

		$form->submit( 'run' )
			->value( __( 'Till tävlingen' ) );

		if ( $form->is_posting() )
		{
			$form->post();
			$form->use_post_values();
		}

		return $form;
	}

	/**
		@brief		The main index.
		@since		2018-04-07 21:01:46
	**/
	public function index()
	{
		$form = $this->get_form();

		Competition::prune_old();
		\App\Models\Participant::prune_old();

		return view( 'index',
			[
				'form' => $form,
			]
		);
	}

	/**
		@brief		Handle the posting of the form.
		@since		2018-04-07 21:37:05
	**/
	public function index_post()
	{
		$form = $this->get_form();

		$competition_key = $form->input( 'competition_key' )->get_filtered_post_value();
		$competition = Competition::maybe_create( $competition_key );

		$password = $form->input( 'password' )->get_filtered_post_value();
		session( [ $competition_key . '_password' => $password ] );

		// We have to check this now since letting the middleware check it will lose the POST.
		if ( ! $competition->verify_password( $password ) )
		{
			$alert = alerts()->alert();
			$alert->set_message( 'Tävlingens lösenord är fel!' );
			return $this->index();
		}

		return redirect()->route( 'competition.get', [ 'competition_key' => $competition_key ] );
	}
}
