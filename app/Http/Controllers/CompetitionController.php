<?php

namespace App\Http\Controllers;

use App\Models\Competition;
use App\Models\Participant;
use Exception;
use Illuminate\Support\Facades\Auth;
use PhpOffice\PhpSpreadsheet\IOFactory;

/**
	@brief		Handles competitions.
	@since		2018-04-07 22:55:00
**/
class CompetitionController extends Controller
{
	/**
		@brief		Open up this competition.
		@since		2018-04-07 22:54:31
	**/
	public function get( $competition_key )
	{
		// Find this competiton.
		$competition = Competition::where( 'key', $competition_key )->first();
		$view_data = [];

		$group = $competition->get_group();

		$settings_form = $this->form();
		$settings_form->id( 'settings' );

		$settings_form->text( 'competition_name' )
			->label( 'Tävlingens namn' )
			->placeholder( '' );

		$settings_form->text( 'competition_section' )
			->label( 'Sektion' );

		$lanes_form = $this->form();
		$lanes_form->id( 'lanes' );
		$lanes_form->number( 'lanes_count' )
			->label( 'Antal banor' )
			->min( 1, 50 );

		$handicaps_form = $this->form();
		$handicaps_form->id( 'handicaps' );
		foreach( $competition->meta()->get_json( 'handicaps' ) as $weapon_type => $handicap )
		{
			$handicaps_form->number( 'handicap_' . $weapon_type )
				->label( $weapon_type )
				->min( -10, 10 )
				->step( 1 )
				->placeholder( $handicap );
				// Don't bother setting the value here since it is all cleared upon init.
		}

		$handicaps_form->checkbox( 'personal_handicaps' )
			->description( 'Ange ett handikapptal per serie för varje skytt vid inskrivning.' )
			->label( 'Personliga handikapp' );

		$settings_series_form = $this->form();
		$settings_series_form->id( 'settings_series' );

		$settings_series_form->number( 'shots' )
			->description( 'Hur många skott som ingår i serien.' )
			->label( 'Skott' )
			->min( 0, 10 )
			->value( 5 );

		$settings_series_form->checkbox( 'next_lane' )
			->checked( true )
			->description( 'Byt till nästa bana efter poängen sparats.' )
			->label( 'Nästa bana' );

		$settings_series_form->number( 'time' )
			->description( 'Om du vill skjuta en annan tid än standardtid, ange den här. Lämna tomt för att behålla standardtiden för serien.' )
			->label( 'Tid' )
			->min( 0, 60 * 5 );

		// Registration
		$registration_form = $this->form();
		$registration_form->id( 'registration' );

		$registration_form->hidden_input( 'participant_id' )
			->value( 0 );

		if ( $group )
		{
			$qs = $group->meta()->get_json( 'participant_quickselect' );
			// Put the quickselect into the export.
			if ( is_array( $qs ) )
			{
				$qs_input = $registration_form->select( 'participant_quickselect' )
					->label( 'Deltagarsnabbval' )
					->opt( '', 'Välj deltagare...' );
				foreach( $qs as $participant )
				{
					// The first key is the participant name.
					$key = array_keys( (array)$participant );
					$key = reset( $key );
					$qs_input->opt( $participant->$key, $participant->$key );
					$opt = $qs_input->option( $participant->$key );
					foreach( $participant as $key => $value )
						$opt->data( $key, $value );
				}
			}
		}

		$registration_form->text( 'participant_name' )
			->label( 'Namn' )
			->placeholder( 'Sven S' );

		$registration_form->select( 'lane' )
			->label( 'Bana' )
			->required();

		$registration_form->select( 'weapon_group' )
			->label( 'Vapengrupp' )
			->required();
		$registration_form->select( 'participant_class' )
			->label( 'Klass' )
			->required();
		$registration_form->select( 'price' )
			->label( 'Betalning' )
			->required();
		$ph = $registration_form->number( 'personal_handicap' )
			->label( 'Handikapp' )
			->min( -50, 50 )
			->value( 0 );

		$series_form = $this->form();
		$series_form->id( 'series' );
		$series_form->radios( 'series_template' )
			->label( 'Serietyp' )
			->opt( 'field', 'Fält' )
			->opt( 'points', 'Poäng' );

		// Send Email
		$send_email_tools_form = $this->form();
		$send_email_tools_form->id( 'send_email' );

		$fs = $send_email_tools_form->fieldset( 'fs_email' );
		$fs->legend->label( 'E-posta resultaten' );

		$message = $fs->textarea( 'message' )
			->description( 'Den här texten kommer att stå med i e-posttexten.' )
			->label( 'Meddelande' )
			->rows( 5, 40 )
			->value( "Se bifogade tävlingsresultat!" );

		$email_recipients = $fs->textarea( 'email_recipients' )
			->description( 'Till vilka adresser ska resultaten skickas' )
			->label( 'E-postadresser' )
			->rows( 5, 40 );
		if ( $group )
			$email_recipients->value( $group->meta()->get( 'email_recipients' ) );

		$fs->button( 'email_send' )
			->value( 'Skicka' );

		if ( $group )
		{
			$p1_api_key = $group->meta()->get( 'p1_api_key' );
			if ( $p1_api_key )
				$fs->button( 'send_to_www' )
					->value( 'Skicka till hemsidan' );
		}

		$tools_password_form = $this->form();
		$tools_password_form->id( 'lock_competition' );

		$fs = $tools_password_form->fieldset( 'fs_password' );
		$fs->legend->label( 'Lösenord' );

		$fs->checkbox( 'write_password' )
			->description( 'x' )		// It must be _something_ else the div won't show up.
			->label( 'Lösenord krävs för att redigera tävlingen' );

		$fs->checkbox( 'read_password' )
			->description( 'x' )		// It must be _something_ else the div won't show up.
			->label( 'Lösenord krävs för att visa tävlingen' );

		$pp = $competition->meta()->get_json( 'preloaded_participants' );
		if ( $pp )
		{
			$preloaded_participants_form = $this->form();
			$preloaded_participants_form->id( 'preloaded_participants' );

			$input = $preloaded_participants_form->select( 'preloaded_participants' )
				->label( 'Ladda förladdade deltagare' )
				->opt( '', 'Välj grupp att ladda...' );

			foreach( $pp as $group_id => $ignore )
				$input->opt( $group_id, 'Grupp ' . $group_id );

			$view_data[ 'preloaded_participants_form' ] = $preloaded_participants_form;
		}

		$competition_quicksettings_form = $this->form();
		$competition_quicksettings_form->id( 'competition_quicksettings' );
		$select = $competition_quicksettings_form->select( 'quicksetting' )
			->label( 'Snabbinställningar' )
			->opt( '', 'Välj snabbinställningar' );

		require_once( __DIR__ . '/quicksettings.php' );
		foreach( $quicksettings as $label => $settings )
		{
			$select->opt( $label, $label );
			$option = $select->options[ $label ];
		}

		$view_data[ 'competition' ] = $competition;
		$view_data[ 'competition_quicksettings_form' ] = $competition_quicksettings_form;
		$view_data[ 'competition_settings_form' ] = $settings_form;
		$view_data[ 'settings_series_form' ] = $settings_series_form;
		$view_data[ 'competition_lanes_form' ] = $lanes_form;
		$view_data[ 'competition_handicaps_form' ] = $handicaps_form;
		$view_data[ 'competition_series_form' ] = $series_form;
		$view_data[ 'registration_form' ] = $registration_form;
		$view_data[ 'send_email_tools_form' ] = $send_email_tools_form;
		$view_data[ 'tools_password_form' ] = $tools_password_form;

		return view( 'competition', $view_data );
	}

	/**
		@brief		Retrieve this competition.
		@details	Used to extract the second parameter, which is the comp key in an api call.
		@since		2019-04-28 11:50:24
	**/
	public function get_api( $api_key, $competition_key )
	{
		return $this->get( $competition_key );
	}

	/**
		@brief		get_participants_txt
		@since		2018-06-21 19:47:03
	**/
	public function get_participants_txt( $competition_key )
	{
		$competition = Competition::where( 'key', $competition_key )->firstOrFail();

		header( 'Content-Type: text/plain' );
		header( 'Content-Disposition: inline; filename="Deltagare ' . $competition->get_filename( 'txt' ) . '"' );
		header( 'Cache-Control: max-age=0' );

		echo $competition->export_participants();

		exit;
	}

	/**
		@brief		Return a Openoffice Spreadsheet of the results.
		@since		2018-04-12 22:30:43
	**/
	public function get_results_ods( $competition_key )
	{
		$competition = Competition::where( 'key', $competition_key )->firstOrFail();
		$spreadsheet = $competition->get_spreadsheet();

		$writer = IOFactory::createWriter( $spreadsheet, 'Ods' );

		// Redirect output to a client’s web browser (Ods)
		header( 'Content-Type: application/vnd.oasis.opendocument.spreadsheet' );
		header( 'Content-Disposition: inline; filename="' . $competition->get_filename( 'ods' ) . '"' );
		header( 'Cache-Control: max-age=0' );

		$writer->save( 'php://output' );
		exit;
	}

	/**
		@brief		Return the PDF results.
		@since		2018-04-13 23:18:29
	**/
	public function get_results_pdf( $competition_key )
	{
		$competition = Competition::where( 'key', $competition_key )->firstOrFail();
		$spreadsheet = $competition->get_spreadsheet();
		$writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter( $spreadsheet, 'Mpdf' );

		// This is because mpdf likes to try and delete the whole temp dir, and will encounter files it is not allowed to delete.
		$temp_dir = storage_path( 'app/cache/mpdf' );
		if ( ! is_dir( $temp_dir ) )
			mkdir( $temp_dir, 0777, true );
		$writer->setTempDir( $temp_dir  );

		header( 'Content-Type: application/pdf' );
		header( 'Content-Disposition: inline; filename="' . $competition->get_filename( 'pdf' ) . '"' );
		$writer->save( 'php://output' );
		exit;
	}

	/**
		@brief		Return the results as text.
		@since		2018-06-21 19:25:59
	**/
	public function get_results_txt( $competition_key )
	{
		$competition = Competition::where( 'key', $competition_key )->firstOrFail();

		header( 'Content-Type: text/plain' );
		header( 'Content-Disposition: inline; filename="' . $competition->get_filename( 'txt' ) . '"' );
		header( 'Cache-Control: max-age=0' );

		echo $competition->export_results_txt();

		exit;
	}

	/**
		@brief		Return an XLS Spreadsheet of the results.
		@since		2018-04-12 22:30:43
	**/
	public function get_results_xls( $competition_key )
	{
		$competition = Competition::where( 'key', $competition_key )->firstOrFail();
		$spreadsheet = $competition->get_spreadsheet();

		$writer = IOFactory::createWriter( $spreadsheet, 'Xls' );

		header( 'Content-Type: application/vnd.ms-excel' );
		header( 'Content-Disposition: inline; filename="' . $competition->get_filename( 'xls' ) . '"' );
		header( 'Cache-Control: max-age=0' );

		$writer->save( 'php://output' );
		exit;
	}

	/**
		@brief		Return a json object that contains the state of the Competition.
		@since		2018-04-07 23:15:57
	**/
	public function get_state( $competition_key )
	{
		$competition = Competition::where( 'key', $competition_key )->firstOrFail();
		try
		{
			$state = $competition->get_state();
			$main_hash = request()->input( 'main_hash' );
			if ( $main_hash == $state->collection( 'hashes' )->get( 'main' ) )
				return response()->json( $main_hash );
			return response()->json( $state->to_array() );
		}
		catch( Exception $e )
		{
			app()->log()->debug( 'Exception: %s', $e->getMessage() );
		}
	}

	/**
		@brief		Return a json object that contains the state of the Competition.
		@details	Used to extract the second parameter, which is the comp key in an api call.
		@since		2019-04-28 22:20:24
	**/
	public function get_state_api( $api_key, $competition_key )
	{
		return $this->get_state( $competition_key );
	}

	/**
		@brief		Send the results to e-mail addresses.
		@since		2018-04-13 07:21:44
	**/
	public function send_email( $competition_key )
	{
		try
		{
			$competition = Competition::where( 'key', $competition_key )->firstOrFail();
			$name = $competition->meta()->get( 'name' );
			$addresses = request()->input( 'addresses' );
			$message = request()->input( 'message' );

			// Split the textarea
			$addresses = \plainview\sdk\base::textarea_to_array( $addresses );

			// Validate each address.
			foreach( $addresses as $index => $address )
				if ( ! \plainview\sdk\base::is_email( $address ) )
					unset( $addresses[ $index ] );

			if ( count( $addresses ) < 1 )
				throw new Exception( 'Inga giltiga e-postadresser hittades.' );

			$mail = \App\Classes\Mail::create();

			$mail->from( 'edward@plainview.se', 'LSF Protocol1' );
			$mail->subject( 'Tävlingsresultat: %s', $name );
			foreach( $addresses as $address )
				$mail->to( $address, $address );

			$mail->text( $message );

			// Generate the data.
			$spreadsheet = $competition->get_spreadsheet();

			$filepaths = [];

			$writer = IOFactory::createWriter( $spreadsheet, 'Mpdf' );
			$filepath = tempnam(sys_get_temp_dir(), 'p1_pdf_' );
			$filepath .= '.pdf';
			$filepaths []= $filepath;
			$temp_dir = storage_path( 'app/cache/mpdf' );
			if ( ! is_dir( $temp_dir ) )
				mkdir( $temp_dir, 0777, true );
			$writer->setTempDir( $temp_dir  );
			$writer->save( $filepath );
			$mail->attachment( $filepath, $competition->get_filename( 'pdf' ) );

			$writer = IOFactory::createWriter( $spreadsheet, 'Xls' );
			$filepath = tempnam(sys_get_temp_dir(), 'p1_xls_' );
			$filepath .= '.xls';
			$filepaths []= $filepath;
			$filename = sprintf( '%s.xls', $name );
			$writer->save( $filepath );
			$mail->attachment( $filepath, $competition->get_filename( 'xls' ) );

			// Create a list of participants for the activity list (flitlistan).
			$filepath = tempnam(sys_get_temp_dir(), 'p1_flit_' );
			$filepath .= '.txt';
			$filepaths []= $filepath;
			$r = $competition->export_participants();
			file_put_contents( $filepath, utf8_decode( $r ) );
			$mail->attachment( $filepath, sprintf( 'Flitlistan %s.txt', date( 'Y-m-d' ) ) );

			// Attach the results.txt
			$filepath = tempnam(sys_get_temp_dir(), 'p1_results_' );
			$filepath .= '.txt';
			$filepaths []= $filepath;
			$r = $competition->export_results_txt();
			file_put_contents( $filepath, utf8_decode( $r ) );
			$mail->attachment( $filepath, $competition->get_filename( 'txt' ) );

			$mail->send();

			foreach( $filepaths as $filepath )
				unlink( $filepath );

			if( ! $mail->send_ok() )
				throw new Exception( 'Kunde inte skicka e-posten!' );

			return response()->json( [
				'status' => 'ok',
				'message' => sprintf( 'Skickat! %s', time() ),
			] );
		}
		catch( Exception $e )
		{
			return response()->json( [
				'status' => 'failure',
				'message' => $e->getMessage(),
			], 400 );
		}
	}

	/**
		@brief		Ask WWW to fetch the results.
		@since		2019-04-28 22:30:07
	**/
	public function send_to_www( $competition_key )
	{
		try
		{
			$competition = Competition::where( 'key', $competition_key )->firstOrFail();

			$group = $competition->get_group();

			$p1_api_key = $group->meta()->get( 'p1_api_key' );
			$p1_api_key_data = base64_decode( str_replace( '_', '/', $p1_api_key ) );
			$p1_api_key_data = json_decode( $p1_api_key_data );
			$url = $p1_api_key_data->url . '&action=maybe_fetch_pdf&code=' . $competition_key;

			$client = new \GuzzleHttp\Client();
			$response = $client->request( 'GET', $url );
			$contents = $response->getBody()->getContents();
			$json = json_decode( $contents );
			if ( $json->result != 'ok' )
				throw new Exception( $contents );

			return response()->json( [
				'status' => 'ok',
				'message' => sprintf( 'Skickat och hämtat! %s', $json->message ),
			] );
		}
		catch( Exception $e )
		{
			return response()->json( [
				'status' => 'failure',
				'message' => $e->getMessage(),
			], 400 );
		}
	}

	/**
		@brief		Update this competition.
		@since		2018-04-08 18:58:20
	**/
	public function set_state( $competition_key )
	{
		$competition = Competition::where( 'key', $competition_key )->firstOrFail();
		$old_state = $competition->get_state( $competition_key );

		$touch = false;

		// This must be first so that everything else works from a fresh reset.
		$data = request()->input( 'reset_competition' );
		if ( $data )
		{
			$competition->generate_default_meta();
			$touch = true;
		}

		// Allow the group to be set.
		$data = request()->input( 'group' );
		if ( $data )
		{
			$competition->meta()->set( 'group', $data );
			$touch = true;
		}

		$competition_settings = request()->input( 'info' );
		if ( is_array( $competition_settings ) )
		{
			foreach( [ 'name', 'section' ] as $key )
			{
				// Key must exist.
				if ( ! isset( $competition_settings[ $key ] ) )
					continue;
				// And be something.
				if ( $competition_settings[ $key ] == '' )
					continue;
				$competition->meta()->set( $key, $competition_settings[ $key ] );
				$touch = true;
			}
			foreach( [ 'read_password', 'write_password' ] as $key )
			{
				// Key must exist.
				if ( ! array_key_exists( $key, $competition_settings ) )
					continue;
				$value = (string) $competition_settings[ $key ];		// The database doesn't like nulls.
				$competition->meta()->set( $key, $value );
				session( [ $competition_key . '_password' => $value ] );
				$touch = true;
			}
		}

		$lanes = request()->input( 'competition_lanes_available' );
		if ( is_array( $lanes ) )
			if ( count( $lanes ) > 0 )
			{
				$competition->meta()->set_json( 'lanes_available', $lanes );
				$touch = true;
			}

		$lanes = request()->input( 'competition_lanes_count' );
		if ( $lanes != '' )
		{
			$lanes = intval( $lanes );
			$competition->meta()->set_json( 'lanes_count', $lanes );
			$touch = true;
		}

		$lanes = request()->input( 'competition_lanes_disabled' );
		if ( is_array( $lanes ) )
			if ( count( $lanes ) > 0 )
			{
				$competition->meta()->set_json( 'lanes_disabled', $lanes );
				$touch = true;
			}

		$competition_handicaps = request()->input( 'competition_handicaps' );
		if ( is_array( $competition_handicaps ) )
		{
			// Sanitize the values.
			foreach( $competition_handicaps as $index => $value )
				$competition_handicaps[ $index ] = intval( $value );
			// And save them.
			$competition->meta()->set( 'handicaps', $competition_handicaps );
			$touch = true;
		}

		$participants = Participant::where( 'competition_id', $competition->id )->get();
		// Only allow these values to be modified if the competition has not begun.
		if ( count( $participants ) < 1 )
		{
			$competition_classes = request()->input( 'competition_classes' );
			if ( is_array( $competition_classes ) )
			{
				uksort( $competition_classes, "strnatcasecmp" );
				$competition->meta()->set( 'classes', $competition_classes );
				$touch = true;
			}

			$competition_prices = request()->input( 'competition_prices' );
			if ( is_array( $competition_prices ) )
			{
				// Sanitize the values.
				foreach( $competition_prices as $description => $price )
				{
					$description = htmlspecialchars( $description );
					$price = intval( $price );
					$competition_prices[ $description ] = $price;
				}
				uksort( $competition_prices, "strnatcasecmp" );
				$competition->meta()->set( 'prices', $competition_prices );
				$touch = true;
			}

			$competition_series = request()->input( 'competition_series' );
			if ( is_array( $competition_series ) )
			{
				$competition->meta()->set( 'series', $competition_series );
				$touch = true;
			}
		}

		$personal_handicaps = request()->input( 'personal_handicaps' );
		if ( $personal_handicaps )
		{
			$competition->meta()->set( 'personal_handicaps', $personal_handicaps );
			$touch = true;
		}

		$load_preloaded_participants = request()->input( 'load_preloaded_participants' );
		if ( $load_preloaded_participants )
		{
			$preloaded_participants = $competition->meta()->get_json( 'preloaded_participants' );
			foreach( (array)$preloaded_participants->$load_preloaded_participants as $participant )
			{
				request()->replace( [
					'register_participant' => (array)$participant,
				] );
				$this->set_state( $competition_key );
			}
		}

		$load_quicksettings = request()->input( 'load_quicksettings' );
		if ( $load_quicksettings )
		{
			require_once( __DIR__ . '/quicksettings.php' );
			$new_settings = array_merge( [ 'reset_competition' => true ], $quicksettings[ $load_quicksettings ] );
			request()->replace( $new_settings );
			$this->set_state( $competition_key );
		}

		$preloaded_participants = request()->input( 'preloaded_participants' );
		if ( $preloaded_participants )
		{
			$competition->meta()->set( 'preloaded_participants', $preloaded_participants );
			$touch = true;
		}

		$data = request()->input( 'register_participant' );
		if ( is_array( $data ) )
		{
			$data = app()->collection( $data );
			// Editing an existing participant?
			$participant_id = intval( $data->get( 'participant_id' ) );
			if ( $participant_id > 0 )
				$p = $competition->get_participant_by_id( $participant_id );
			else
				$p = $competition->get_participant( $data->get( 'name' ) );

			$lane_id = $data->get( 'lane' );
			$old_lane_id = -1;

			if ( $lane_id == 'delete' )
			{
				$p->delete();
				$touch = true;
			}

			if ( ! $touch )
			{
				if ( ! $p )
				{
					// New participant
					$p = new Participant();
					$p->competition_id = $competition->id;
					$p->save();

					app()->log()->debug( 'Adding new participant: %s', var_export( $data, true ) );

					// Everybody is a shooter to start off with.
					$p->meta()->set( 'shooter', true );

					if ( $data->get( 'lane' ) == 'queued' )
						$p->meta()->set( 'queued', time() );

					if ( $data->get( 'lane' ) == 'F' )
					{
						$p->meta()->set( 'official', true );
						$p->meta()->set( 'shooter', false );
					}

					if ( $data->get( 'lane' ) == 'T' )
					{
						$p->meta()->set( 'practice', true );
						$p->meta()->set( 'shooter', false );
					}

					$p->meta()->set( 'competition_id', $p->competition_id );

					if ( $lane_id > 0 )
						$p->meta()->set( 'busy', true );
				}
				else
				{
					// Existing participant
					$old_lane_id = $p->meta()->get( 'lane' );
				}

				// Handle the lane.
				if ( $lane_id > 0 )
				{
					if ( $old_lane_id !== -1 )
					{
						if ( $old_lane_id != $lane_id )
							$competition->switch_lanes( $old_lane_id, $lane_id );
					}
					else
						$p->meta()->set( 'lane', $lane_id );
				}

				$p->meta()->set( 'name', $data->get( 'name' ) );

				// If no class set, set the first one available.
				if ( $data->get( 'participant_class' ) == '' )
				{
					$classes = (array) $old_state->get( 'classes' );
					$class = key( $classes );
					$data->set( 'participant_class', $class );
					app()->log()->debug( 'Setting new participant class for %s: %s', $data->get( 'name' ), $class );
				}
				$p->meta()->set( 'participant_class', $data->get( 'participant_class' ) );

				$p->meta()->set( 'personal_handicap', $data->get( 'personal_handicap', 0 ) );

				$prices = $competition->meta()->get_json( 'prices' );
				$price = $data->get( 'price' );
				if ( ! $price )
					$price = key( (array)$prices );
				else
					$price = $prices->$price;
				$p->meta()->set( 'price_text', $data->get( 'price', '' ) );
				$p->meta()->set( 'price', $price );

				$p->meta()->set( 'weapon_group', $data->get( 'weapon_group' ) );
				$touch = true;
			}
		}

		$save_score = request()->input( 'save_score' );
		if ( is_array( $save_score ) )
		{
			$p = $competition->get_participant( $save_score[ 'participant_name' ] );
			if ( $p )
			{
				// Found it!
				$key = sprintf( 'score_%d', $save_score[ 'series' ] );
				$p->meta()->set( $key, $save_score[ 'score' ] );
				$key = sprintf( 'score_meta_%d', $save_score[ 'series' ] );
				$p->meta()->set( $key, $save_score[ 'score_meta' ] );
				if ( isset( $save_score[ 'finish' ] ) )
				{
					$p->meta()->set( 'finished', true );
					$p->meta()->delete( 'busy' );
					$competition->unqueue();
				}
				$touch = true;
			}
		}

		$unregister_participant = request()->input( 'unregister_participant' );
		if ( is_array( $unregister_participant ) )
		{
			$p = $competition->get_participant_by_id( $unregister_participant[ 'participant_id' ] );

			if ( ! $p )
				throw new Exception( 'Participant not found!' );

			// May we unregister this participant?
			$permanent = false;
			$permanent |= ( $p->meta()->get( 'score_1' ) !== false );
			$permanent |= $p->meta()->get( 'finished' );
			if ( ! $permanent )
			{
				$p->delete();
				$touch = true;
			}
		}

		if ( $touch )
			$competition->touch();

		return $this->get_state( $competition_key );
	}

	/**
		@brief		Set the state using an API call.
		@details	Used to extract the second parameter, which is the comp key in an api call.
		@since		2019-04-28 11:50:24
	**/
	public function set_state_api( $api_key, $competition_key )
	{
		return $this->set_state( $competition_key );
	}

	/**
		@brief		When reloading a subpage in the comp, redirect to the comp front page.
		@since		2018-10-20 18:39:11
	**/
	public function redirect_to_competition( $competition_key )
	{
	return redirect()->route( 'competition.get', [ 'competition_key' => $competition_key ] );
	}
}
