<?php

namespace App\Models;

/**
	@brief		Base model class.
	@since		2018-04-08 01:55:44
**/
class Model
	extends \Illuminate\Database\Eloquent\Model
{
	/**
		@brief		Return a meta handler.
		@since		2018-04-12 15:41:06
	**/
	public function meta()
	{
		return \App\Helpers\Meta::from_model( $this );
	}
}
