<?php

namespace App\Models;

/**
	@brief		Meta for all models.
	@since		2018-04-08 01:55:05
**/
class Meta extends Model
{
	protected $table = 'meta';
}
