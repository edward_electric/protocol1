<?php

namespace App\Models;

/**
	@brief		A participant in the competition.
	@since		2018-04-11 12:48:28
**/
class Participant extends Model
{
	/**
		@brief		Prune this participant.
		@since		2018-10-06 15:31:23
	**/
	public function prune()
	{
		$this->meta()->delete_all();
		$this->delete();
	}

	/**
		@brief		Remove orphan participants.
		@since		2018-10-06 15:18:16
	**/
	public static function prune_old()
	{
		$competition_ids = Competition::select( 'id' )->pluck( 'id' );
		$old = static::whereNotIn( 'competition_id', $competition_ids )->get();
		foreach( $old as $participant )
			$participant->prune();

		$participant_ids = Participant::select( 'id' )->pluck( 'id' );
		Meta::where( 'object_type', 'Participant' )
			->whereNotIn( 'object_id', $participant_ids )
			->delete();
	}

	/**
		@brief		Convert the participant to a simple array containing all data + meta.
		@since		2018-06-06 22:10:34
	**/
	public function to_object()
	{
		$r = (object)[];
		$r->id = $this->id;
		$meta = $this->meta()->get_all();
		foreach( $meta as $key => $value )
			$r->$key = $value;
		return $r;
	}
}
