<?php

namespace App\Models;

/**
	@brief		Used for communicating with the LSF P1 plugin.
	@since		2019-04-26 11:18:56
**/
class P1_API
{
	/**
		@brief		The competition model we are assigned to.
		@since		2019-04-26 11:19:41
	**/
	public $competition;

	/**
		@brief		The LSF P1 API data.
		@since		2019-04-26 11:19:22
	**/
	public $key;

	/**
		@brief		Constructor.
		@since		2019-04-26 11:19:16
	**/
	public function __construct( $competition )
	{
		$this->competition = $competition;
		$this->key = $competition->p1_api_key;
	}
}
