<?php

namespace App\Models;

use App\Models\Group;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

/**
	@brief		Model for handling of competitions.
	@since		2018-04-07 22:14:13
**/
class Competition extends Model
{
	/**
		@brief		Export the participants as text.
		@since		2018-06-21 19:27:25
	**/
	public function export_participants()
	{
		$r = '';
		$state = $this->get_state();
		$gp = $state->participants;
		$r = [];
		foreach( $gp->collection( 'all' ) as $participant )
		{
			// Participant info.
			$info = [ $participant->name ];

			// Decide what to show as the second column, the class.
			switch( $participant->participant_class )
			{
				case 'F':
					$info []= 'F';
				break;
				default:
					$info []= $participant->weapon_group;
				break;
			}

			// Add any series if possible.
			if ( $participant->precision_total_with_handicap > 0 )
			{
				$info []= $participant->precision_total_with_handicap;
				// Reload the part for the meta.
				$participant = $this->get_participant( $participant->name );
				$series = 0;
				do
				{
					$series++;
					$key = sprintf( 'score_meta_%d', $series );
					$meta = $participant->meta()->get( $key );
					if ( ! $meta )
						break;

					// Ignore series without scores.
					$meta = json_decode( $meta );
					if ( ! is_array( $meta ) )
						break;

					// Remove null arrays.
					$meta = array_filter( $meta );
					if ( count( $meta ) < 1 )
						continue;

					$meta = json_encode( $meta );
					$info []= $meta;
				} while ( true );
			}

			$r []= implode( "\t", $info );
		}
		sort( $r );
		$r = implode( "\n", $r );
		$r .= "\n";
		return $r;
	}

	/**
		@brief		export_results_txt
		@since		2018-06-21 19:48:52
	**/
	public function export_results_txt()
	{
		$r = '';
		$state = $this->get_state();
		$gp = $state->participants;
		$r = [];
		foreach( $gp->collection( 'all' ) as $participant )
		{
			if ( ! isset( $participant->precision_total_with_handicap ) )
				return;
			$r []= sprintf( "%s\t%s", $participant->name, $participant->precision_total_with_handicap );
		}
		sort( $r );
		$r = implode( "\n", $r );
		$r .= "\n";
		return $r;
	}

	/**
		@brief		Generate some default meta for this model.
		@since		2018-04-08 12:28:04
	**/
	public function generate_default_meta()
	{
		$this->meta()->set( 'classes', [
			'Med licens' => 'ML',
			'Utan licens' => 'UL',
			'Funktionärer' => 'F',
			'Träning' => 'T',
		] );

		// This doubles as the list of weapon types.
		$this->meta()->set( 'handicaps', [
			'A' => 0,
			'B' => 0,
			'C' => 0,
			'R' => 0,
			'M' => 0,
		] );

		$this->meta()->set( 'lanes_available', range( 1, 24 ) );
		$this->meta()->set( 'lanes_count', 24 );
		$this->meta()->set( 'lanes_disabled', [] );
		$this->meta()->set( 'prices', [
			'Abonnemang 0kr' => 0,
			'Funktionär' => 0,
			'Med pistol 50kr' => 50,
			'Utan pistol 100kr' => 100,
			'Vapendragare 0kr' => 0,
		] );
		$this->meta()->set( 'name', date( 'Y-m-d H:i:s' ) );
		$this->meta()->set( 'section', 'Pistol' );
		$this->meta()->set( 'series', [
			[
				'type' => 'precision',
				'shots' => 5,
			],
			[
				'type' => 'precision',
				'shots' => 5,
			],
			[
				'type' => 'precision',
				'shots' => 5,
			],
			[
				'type' => 'precision',
				'shots' => 5,
			],
			[
				'type' => 'precision',
				'shots' => 5,
			],
			[
				'type' => 'precision',
				'shots' => 5,
			],
			[
				'type' => 'precision',
				'shots' => 5,
			],
			[
				'type' => 'precision',
				'shots' => 5,
			],
			[
				'type' => 'precision',
				'shots' => 5,
			],
			[
				'type' => 'precision',
				'shots' => 5,
			],
			[
				'type' => 'precision',
				'shots' => 5,
			],
			[
				'type' => 'precision',
				'shots' => 5,
			],
		] );
	}

	/**
		@brief		Generate the base filename for this competition.
		@since		2018-04-26 22:59:58
	**/
	public function get_filename( $ext = '' )
	{
		$r = sprintf( '%s %s', $this->get_updated_at_date(), $this->meta()->get( 'name' ) );
		if ( $ext != '' )
			$r .= '.' . $ext;
		return $r;
	}

	/**
		@brief		Return an unused random key.
		@since		2018-04-07 22:43:06
	**/
	public static function get_new_key()
	{
		do
		{
			$key = static::random_key();
			$results = static::where( 'key', $key )->get();
		} while( count( $results ) > 0 );
		return $key;
	}

	/**
		@brief		Load a participant.
		@since		2018-04-16 21:41:22
	**/
	public function get_participant( $participant_name )
	{
		$participants = Participant::where( 'competition_id', $this->id )->get();
		foreach( $participants as $p )
		{
			$meta = $p->meta()->get_all();
			if ( ! isset( $meta[ 'name' ] ) )
				continue;		// How is this possible, though?
			if ( $meta[ 'name' ] != $participant_name )
				continue;
			return $p;
		}
		return false;
	}

	/**
		@brief		Return a participant by his ID.
		@since		2018-06-06 22:19:43
	**/
	public function get_participant_by_id( $participant_id )
	{
		return Participant::where( 'competition_id', $this->id )
			->where( 'id', $participant_id )
			->get()
			->first();
	}

	/**
		@brief		Return the state of this competition as an array.
		@since		2018-04-07 23:19:37
	**/
	public function get_state()
	{
		$state = new State();
		$state->set_competition( $this );

		$state->set( 'url', Route( 'competition.get', [ 'id' => $this->key ] ) );

		$group_id = $this->meta()->get( 'group', 0 );
		$state->set( 'group', $group_id );

		$state->set( 'key', $this->key );															// Unique key of this competition.
		$state->set( 'classes',					$this->meta()->get_json( 'classes' ) );				// Array of classes into which the shooters are categorised.
		$state->set( 'prices',					$this->meta()->get_json( 'prices' ) );				// How much a participant can pay.
		$state->collection( 'handicaps' )->set( 'handicaps',				$this->meta()->get_json( 'handicaps', [] ) );			// How many points to handicap the various weapon groups.
		$state->collection( 'handicaps' )->set( 'personal_handicaps',		$this->meta()->get_json( 'personal_handicaps' ) );	// Are personal handicaps enabled?
		$state->collection( 'info' )->set( 'name',						$this->meta()->get( 'name' ) );						// The name / description of the competition.
		$state->collection( 'info' )->set( 'section',					$this->meta()->get( 'section' ) );					// Type of competition.
		$state->collection( 'info' )->set( 'read_password',				$this->meta()->get( 'read_password' ) );			// The password required to read from the competition.
		$state->collection( 'info' )->set( 'write_password',			$this->meta()->get( 'write_password' ) );			// The password required to write to the competition.
		$pp = (array) $this->meta()->get_json( 'preloaded_participants' );
		if ( $pp )
			$state->set( 'preloaded_participants',	array_keys( $pp ) );		// preloaded_participants from the LSF plugin.
		$state->collection( 'registration_defaults' )->set( 'class',			'ML' );
		$state->collection( 'registration_defaults' )->set( 'price',			'Med pistol 50kr' );
		$state->collection( 'registration_defaults' )->set( 'weapon_group',		'C' );
		$state->collection( 'series' )->set( 'series',					$this->meta()->get_json( 'series' ) );				// Array containing series information.

		$lanes = $state->collection( 'lanes' );
		// We want the lane IDs as both keys and values.
		$lanes_available = $this->meta()->get_json( 'lanes_available' );
		$lanes_available = ( array )$lanes_available;
		$lanes_available = array_combine( $lanes_available, $lanes_available );
		$lanes->set( 'count', $this->meta()->get_json( 'lanes_count' ) );
		$lanes->set( 'disabled', $this->meta()->get_json( 'lanes_disabled' ) );
		$lanes->collection( 'available' )->import_array( $lanes_available );
		$lanes->collection( 'busy' );
		$lanes->collection( 'free' )->import_array( $lanes_available );
		$lanes->collection( 'participants' );

		$lanes->set( 'first_lane', PHP_INT_MAX );					// The first lane that is being used.
		$lanes->set( 'last_lane', 0 );								// The last lane that is being used.

		$state->collection( 'series' )->set( 'has_precision', false );
		$state->collection( 'series' )->set( 'has_field', false );

		// Count the amount of precision series.
		foreach( $state->get_series() as $index => $series )
			if( $series->type == 'precision' )
				$state->collection( 'series' )->set( 'precision_series', $state->collection( 'series' )->get( 'precision_series' ) + 1 );

		$state->collection( 'series' )->set( 'max', 0 );
		$state->set( 'money', 0 );

		$participants = Participant::where( 'competition_id', $this->id )->get();
		$state->participants = collection();		// This is a special key in the state that contains groups collections of participants.
		$state->collection( 'participants' );		// Initialize thise collection so that it exists.
		$state_participants = $state->participants;
		foreach( $participants as $participant )
		{
			$p = $participant->to_object();
			$p_name = $p->name;

			if ( isset( $p->price ) )
				$state->set( 'money', $state->get( 'money' ) + intval( $p->price ) );

			// Assemble the scores.
			$p->field_hits_total = 0;
			$p->field_targets_total = 0;
			$p->last_precision = '';
			$p->precision_series = 0;
			$p->precision_total = 0;
			$p->precision_total_with_handicap = 0;		// This is set to precision_total later, if not handicapped.
			$p->scores = 0;
			foreach( $state->get_series() as $index => $series )
			{
				// Scores are 1 based.
				$index++;
				$key = 'score_' . $index;
				if ( ! isset( $p->$key ) )
					continue;

				$state->collection( 'series' )->set( 'max',
					max( $state->collection( 'series' )->get( 'max' ), $index )
				);

				$p->scores++;

				// Here is a score.
				switch( $series->type )
				{
					case 'field':
						$state->collection( 'series' )->set( 'has_field', true );
						$p->has_field = true;
						$p->field_hits_total += $p->$key;
						// Field targets need to be recorded also.
						$key = 'score_meta_' . $index;
						$p->field_targets_total += $p->$key[ 0 ];
						break;
					case 'precision':
						$p->precision_series++;
						$state->collection( 'series' )->set( 'has_precision', true );
						$p->precision_total += $p->$key;
						// The latest series are the most important, therefore them being put forth to be string sorted by value.
						$p->last_precision = str_pad( $p->$key, 2 , '0', STR_PAD_LEFT ) . $p->last_precision;
						break;
				}
			}

			// Pad the last_precision according to the max amount of series, in order for the scores to be sorted correctly by max score, but not by last precision.
			// *2 because "50" is two characters long.
			$p->last_precision = str_pad( $p->last_precision, count( $state->get_series() ) * 2, '0', STR_PAD_LEFT );

			if ( $state->has_handicaps() )
			{
				$weapon_group = $p->weapon_group;
				$handicap = $state->collection( 'handicaps' )->get( 'handicaps' )->$weapon_group;
				if ( $state->has_personal_handicaps() )
					$handicap += $p->personal_handicap;
				$p->precision_total_with_handicap = $p->precision_total + ( $p->precision_series * $handicap );
			}
			else
				$p->precision_total_with_handicap = $p->precision_total;

			$state_participants->collection( 'classes' )
				->collection( 'all' )
				->set( $p_name, $p );

			$state_participants->collection( 'classes' )
				->collection( $p->participant_class )
				->set( $p_name, $p );

			if ( isset( $p->busy ) )
			{
				// Mark this lane as busy.
				$lane_id = $p->lane;
				$lanes->collection( 'free' )->forget( $lane_id );
				$lanes->collection( 'busy' )->set( $lane_id, [
					'participant_name' => $p_name
				] );
				$lanes->collection( 'participants' )->set( $lane_id, $p_name );
				$lanes->set( 'first_lane', min( $lanes->get( 'first_lane' ), $lane_id ) );
				$lanes->set( 'last_lane', max( $lanes->get( 'last_lane' ), $lane_id ) );
				$state_participants->collection( 'busy' )->set( $p_name, $p );
			}

			if ( isset( $p->queued ) )
				$state_participants->collection( 'queued' )->set( $p_name, $p );

			if ( isset( $p->finished ) )
				$state_participants->collection( 'finished' )->set( $p_name, $p );

			if ( $p->shooter )
			{
				$state_participants->collection( 'shooters' )->set( $p_name, $p );
				$state_participants->collection( 'shooting_classes' )
					->collection( $p->participant_class )
					->set( $p->name, $p );
			}
			else
			{
				$state_participants->collection( 'non_shooters' )->set( $p_name, $p );
				$state_participants->collection( 'non_shooting_classes' )
					->collection( $p->participant_class )
					->set( $p->name, $p );
			}

			$state_participants->collection( 'all' )->set( $p_name, $p );

			$state->collection( 'participants' )->set( $p_name, $p );
		}

		// Generate the registration HTML overview
		// ---------------------------------------

		$html = '';

		$classes = $state->get( 'classes' );
		$classes = (array)$classes;
		$classes = array_flip( $classes );

		$classes[ 'all' ] = 'Alla';

		ksort( $classes );

		$groups = [];

		foreach( $state_participants->collection( 'classes' )->to_array() as $class_id => $ignore )
		{
			if ( ! isset( $classes[ $class_id ] ) )
				continue;
			$groups[ $class_id ] = $classes[ $class_id ];
		}

		ksort( $groups, SORT_STRING | SORT_FLAG_CASE );

		foreach( $groups as $group_id => $group_name )
		{
			$sorted = $state_participants->collection( 'classes' )->get( $group_id )->sort_by( function( $p )
			{
				$sort_key = '';
				foreach( [ 'lane', 'queued', 'name' ] as $key )
				{
					if ( ! isset( $p->$key ) )
						continue;
					$value = $p->$key;
					if ( intval( $value ) > 0 )
						$value = str_pad( $value, 2, '0', STR_PAD_LEFT );
					else
						$value = '99' . $value;
					$sort_key .= $value . '_';
				}
				$sort_key .= $p->name;
				return $sort_key;
			} );
			// Nothing to do? Skip this group.
			if ( count( $sorted ) < 1 )
				continue;
			$table = new \plainview\sdk\table\table();
			$table->id( $group_id );
			$table->caption()->text( $group_name . ' <small>(' . count( $sorted ) . ')</small>' );
			$row = $table->head()->row();
			$row->th( 'name' )->text( 'Namn' );
			$row->th( 'lane' )->text( 'Bana' );
			$row->th( 'series' )->text( 'Serie' );
			$row->th( 'price' )->text( 'Betalt' );
			$row->th( 'class' )->text( 'Klass' );
			$row->th( 'weapon_group' )->text( 'Vapengrupp' );

			if ( $state->collection( 'handicaps' )->get( 'personal_handicaps' ) );
				$row->th( 'personal_handicap' )->text( 'Hkp' );

			foreach( $sorted as $name => $p )
			{
				$row = $table->body()->row();
				$row->css_class( 'editable' );
				$row->set_attribute( 'participant_id', $p->id );

				$row->td( 'name' )->text( $name );

				$text = 'n/a';
				if ( isset( $p->lane ) )
					$text = $p->lane;
				if ( isset( $p->queued ) )
					$text = 'Köad';
				$row->td( 'lane' )->text( $text );

				$td = $row->td( 'series' )->text( $p->scores );
				$td->css_class( 'series_' . $p->scores );

				// Sometimes the price is not set.
				if ( ! isset( $p->price ) )
					$p->price = 0;
				$row->td( 'price' )->text( $p->price );
				$row->td( 'participant_class' )->text( $p->participant_class );
				if ( ! isset( $p->weapon_group ) )
					$p->weapon_group = '';
				$row->td( 'weapon_group' )->text( $p->weapon_group );

				if ( $state->collection( 'handicaps' )->get( 'personal_handicaps' ) );
					$row->td( 'personal_handicap' )->text( $p->personal_handicap );
			}

			$html .= $table . '';
		}

		$state->collection( 'html' )->set( 'participants', $html );

		// Build the HTML results tables
		// -----------------------------

		// Summary
		$html = '';
		$shooting_classes = $state_participants->collection( 'shooting_classes' );
		$shooting_classes->sort_by_key();
		foreach( $shooting_classes as $class_id => $participants )
		{
			// Find the nice class name.
			$class_name = $classes[ $class_id ];
			$table = new \plainview\sdk\table\table();
			$table->caption()->text( $class_name );
			$row = $table->head()->row();
			$row->th( 'rank' )->text( 'Placering' );
			$row->th( 'name' )->text( 'Namn' );
			if ( $state->collection( 'series' )->get( 'has_field' ) )
				$row->th( 'field_points' )
					->css_class( 'points' )
					->text( 'Fält' );
			if ( $state->collection( 'series' )->get( 'has_precision' ) )
			{
				$row->th( 'precision_points' )
					->css_class( 'points' )
					->text( 'Precision' );
				if ( $state->has_handicaps() )
				{
					$row->th( 'precision_total_with_handicap' )
						->css_class( 'points' )
						->text( '+Hkp' );
				}
			}

			$ordered = $this->sort_participants_by_score( $participants );

			$counter = 0;
			foreach( $ordered as $participant )
			{
				$counter++;
				$row = $table->body()->row();
				$row->td( 'rank' )->text( $counter );
				$row->td( 'name' )->text( $participant->name );
			if ( $state->collection( 'series' )->get( 'has_field' ) )
				{
					$text = sprintf( '%s/%s', $participant->field_hits_total, $participant->field_targets_total );
					$row->td( 'field_points' )
						->css_class( 'points' )
						->text( $text );
				}
				if ( $state->collection( 'series' )->get( 'has_precision' ) )
				{
					$row->td( 'precision_points' )
						->css_class( 'points' )
						->text( $participant->precision_total );
					if ( $state->has_handicaps() )
					{
						$row->td( 'precision_total_with_handicap' )
							->css_class( 'points' )
							->text( $participant->precision_total_with_handicap );
					}
				}
			}
			$html .= $table;
		}
		$state->collection( 'html' )->set( 'scores', $html );

		// Detailed scores
		$html = '';
		foreach( $shooting_classes as $class_id => $participants )
		{
			// Find the nice class name.
			$class_name = $classes[ $class_id ];
			$table = new \plainview\sdk\table\table();
			$table->caption()->text( $class_name );
			$row = $table->head()->row();
			$row->th( 'rank' )->text( 'Placering' );
			$row->th( 'name' )->text( 'Namn' );
			$row->th( 'lane' )->text( 'Bana' );
			$row->th( 'price' )->text( 'Betalt' );
			$row->th( 'weapon_group' )->text( 'Vapengrupp' );

			// Assemble the series.
			foreach( $state->get_series() as $index => $series )
			{
				$index++;

				if ( $index > $state->collection( 'series' )->get( 'max' ) )
					break;

				$row->th( 'series_' . $index )
					->css_class( 'points' )
					->text( $index );
			}

			if ( $state->collection( 'series' )->get( 'has_precision' ) )
				if ( $state->has_handicaps() )
					$row->th( 'precision_total_with_handicap' )->text( '+Hkp' );

			$ordered = $this->sort_participants_by_score( $participants );

			$counter = 0;
			foreach( $ordered as $participant )
			{
				$counter++;
				$row = $table->body()->row();
				$row->td( 'rank' )->text( $counter );
				$name = str_replace( ' ', '&nbsp;', $participant->name );
				$row->td( 'name' )->text( $name );
				$text = '';
				if ( isset( $participant->lane ) )
					$text = $participant->lane;
				if ( isset( $participant->queued ) )
					$text = 'Köad';
				$row->td( 'lane' )->text( $text );
				$row->td( 'price' )->text( $participant->price );
				$row->td( 'weapon_group' )->text( $participant->weapon_group );
				foreach( $state->get_series() as $index => $series )
				{
					$index++;
					$key = 'score_' . $index;

					if ( $index > $state->collection( 'series' )->get( 'max' ) )
						break;

					$td = $row->td( 'series_' . $index )
						->css_class( 'points' );

					if ( ! isset( $participant->$key ) )
						$text = '';
					else
					{
						switch( $series->type )
						{
							case 'field':
								$text = $participant->$key;
								$key = 'score_meta_' . $index;
								$text = sprintf( '%s&nbsp;/&nbsp;%s', $text, $participant->$key[ 0 ] );
								break;
							case 'precision':
								if ( $series->shots == 5 )
								{
									$gold = false;

									if ( $participant->weapon_group == 'A' )
										if ( $participant->$key > 42 )
											$gold = true;

									if ( $participant->weapon_group == 'B' )
										if ( $participant->$key > 44 )
											$gold = true;

									if ( $participant->weapon_group == 'C' )
										if ( $participant->$key > 45 )
											$gold = true;

									if ( $participant->$key > 49 )
									{
										$gold = false;
										$td->css_class( 'precision' );
										$td->css_class( 'perfect' );
									}

									if ( $gold )
									{
										$td->css_class( 'precision' );
										$td->css_class( 'gold' );
									}
								}
								$text = $participant->$key;
								break;
						}
					}
					$td->text( $text );
				}

				if ( $state->collection( 'series' )->get( 'has_precision' ) )
					if ( $state->has_handicaps() )
						$row->th( 'precision_total_with_handicap' )->text( $participant->precision_total_with_handicap );

			}
			$html .= $table;
		}
		$state->collection( 'html' )->set( 'detailed_scores', $html );

		$non_shooters = '';

		// Find non shooters
		$nsc = $state_participants->collection( 'non_shooting_classes' );
		foreach( $nsc as $class_id => $participants )
		{
			$class_name = $classes[ $class_id ];
			if ( count( $participants ) < 1 )
				continue;

			$participants = $participants->sort_by( function( $p )
			{
				return $p->name;
			} );

			$non_shooters .= sprintf( '<h3>%s</h3>', $class_name );

			$non_shooters .= '<ul>';

			foreach( $participants as $participant )
				$non_shooters .= '<li>' . $participant->name . '</li>';

			$non_shooters .= '</ul>';
		}
		$html = $state->collection( 'html' )->get( 'detailed_scores' );
		$html .= $non_shooters;
		$state->collection( 'html' )->set( 'detailed_scores', $html );

		$state->collection( 'urls' )->import_array( [
			'get_state' => route( 'competition.get_state', [ 'competition_key' => $this->key ] ),
			'set_state' => route( 'competition.set_state', [ 'competition_key' => $this->key ] ),
			'send_email' => route( 'competition.send_email', [ 'competition_key' => $this->key ] ),
			'send_to_www' => route( 'competition.send_to_www', [ 'competition_key' => $this->key ] ),
			'export_results_pdf' => route( 'competition.export.results.pdf', [ 'competition_key' => $this->key ] ),
		] );

		// Hash all of the data.
		foreach( $state as $key => $values )
			$state->collection( 'hashes' )->set( $key, $this->hash( serialize( $values ) ) );
		$state->collection( 'hashes' )->set( 'main', $this->hash( $state->collection( 'hashes' )->to_array() ) );

		return $state;
	}
	/**
		@brief		Return the group we belong to.
		@since		2019-04-28 12:05:46
	**/
	public function get_group()
	{
		$group_id = $this->meta()->get( 'group' );
		$group = false;
		if ( $group_id > 0 )
			$group = Group::findOrFail( $group_id );
		return $group;
	}

	/**
		@brief		Get a phpspreadsheet.
		@since		2018-04-12 22:47:38
	**/
	public function get_spreadsheet( $data = [] )
	{
		if ( ! is_object( $data ) )
			$data = (object) $data;

		$columns_to_center = [
			'rank_column'			=> 'participants_start_row',
			'weapon_group_column'	=> 'participants_start_row',
		];

		if ( ! isset( $data->crdata ) )
			$data->crdata = (object) [];

		$meta = $this->meta()->get_all();

		$group = $this->get_group();

		$spreadsheet = new Spreadsheet();

		$spreadsheet
			->getProperties()
			->setCreator( 'LSF Protokoll1' )
			->setTitle( 'Tävlingsresultat ' . $meta->get( 'name' ) );

		$spreadsheet->getDefaultStyle()->getFont()->setName( 'Arial' );
		$spreadsheet->getDefaultStyle()->getFont()->setSize( 10 );
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$spreadsheet->setActiveSheetIndex( 0 );
		$sheet = $spreadsheet->getActiveSheet();

		// To help Mpdf, we delete all of the borders.
		if ( isset( $data->second_pass ) )
		{
			$sheet->getStyle( 'A1:' . $data->highest_column . $data->highest_row )
					->applyFromArray( [
						'borders' => [
							'allBorders' => [
								'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_NONE,
							],
						],
					] );
		}

		$sheet->setTitle( 'Resultat' );

		$sheet->getPageSetup()
			->setPaperSize( \PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::PAPERSIZE_A4 );

		$sheet->getPageSetup()->setFitToWidth( 1 );
		$sheet->getPageSetup()->setFitToHeight( 0 );

		$sheet->getPageMargins()->setTop( 1 );
		$sheet->getPageMargins()->setRight( 0.75 );
		$sheet->getPageMargins()->setLeft( 0.75 );
		$sheet->getPageMargins()->setBottom( 1 );

		if ( $group )
			$name = $group->name;
		else
			$name = config( 'app.name' );
		$sheet->setCellValue( 'A1' , $name );
		$sheet->setCellValue( 'A2' , $meta->get( 'section' ) );
		$sheet->setCellValue( 'A3' , $this->get_updated_at_date() );
		// Competition name gets sets lower down when we know how many columns we expect to use.

		$column = 'A';
		$start_column = $column;
		$row = 5;

		$state = $this->get_state();
		$classes = (array) $state->get( 'classes' );
		$classes = array_flip( $classes );

		// Use the grouped participants.
		$shooting_classes = $state->participants->collection( 'shooting_classes' )->to_array();
		ksort( $shooting_classes );

		$max_column = 'C';
		$data->crdata->sums_column_end = $max_column;
		$data->crdata->series_column_end = $max_column;

		foreach( $shooting_classes as $class_id => $participants )
		{
			// Insert a newline.
			$cr = $column . $row;
			$sheet->setCellValue( $cr , ' ' );
			$row++;

			$cr = $column . $row;

			$class_name = $classes[ $class_id ];
			$cr = $column . $row;
			$sheet->setCellValue( $cr , $class_name );

			$sheet
				->getStyle( $cr )
				->applyFromArray( [
					'font' => [
						'italic' => true,
					],
				] );
			// Rank / Class
			$column++;

			// Name
			$column++;

			// Weapon group.
			$column++;

			$data->crdata->series_column_start = $column;
			$data->crdata->series_row_start = $row;

			// Compile the series headings. Sometimes there are seconds, so they should be grouped.
			$old_series_name = '';
			$series_names = [];
			$temp_column = $column;
			foreach( $state->get_series() as $index => $series_data )
			{
				if ( $index >= $state->collection( 'series' )->get( 'max' ) )
					break;

				if ( isset( $series_data->time ) )
					$series_name = $series_data->time . 's';
				else
					$series_name = 'Serier';

				if ( $series_name != $old_series_name )
				{
					$series_names []= (object)[ 'name' => $series_name, 'count' => 0 ];
					$old_series_name = $series_name;
				}
				else
					$series_names[ count( $series_names ) - 1 ]->count++;

				$temp_column++;
				$data->crdata->series_column_end = $temp_column;
			}

			foreach( $series_names as $series_data )
			{
				$cr = $column . $row;

				$style_array = [
					'alignment' => [
						'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
					],
					'borders' => [
						'left' => [
							'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
						],
						'top' => [
							'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
						],
						'right' => [
							'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
						],
					],
					'font' => [
						'italic' => true,
					],
				];

				$sheet
					->getStyle( $cr )
					->applyFromArray( $style_array );

				$sheet->setCellValue( $cr , $series_data->name );

				$end_column = $column;
				for( $counter = 0; $counter < $series_data->count ; $counter++ )
					$end_column++;

				$sheet->mergeCells( $column . $row . ':' . $end_column . $row );
				$column = $end_column;
				$column++;
			}

			$row++;
			$column = $start_column;

			// Mark down the headings.
			if ( ! isset( $data->crdata->headings_rows ) )
				$data->crdata->headings_rows = [];
			$data->crdata->headings_rows []= $row;

			$cr = $column . $row;
			$data->crdata->rank_column = $column;
			$sheet->setCellValue( $cr , 'Placering' );
			$column++;

			$cr = $column . $row;
			$data->crdata->name_column = $column;
			$sheet->setCellValue( $cr , 'Namn' );
			$column++;

			$cr = $column . $row;
			$data->crdata->weapon_group_column = $column;
			$sheet->setCellValue( $cr , 'Vgrp' );
			$column++;

			foreach( $state->get_series() as $index => $ignore )
			{
				if ( $index + 1 > $state->collection( 'series' )->get( 'max' ) )
					break;

				$cr = $column . $row;

				$sheet->setCellValue( $cr , $index + 1 );
				$column++;
			}

			$data->crdata->sums_column_start = $column;
			$data->crdata->sums_column_end = $column;

			if ( $state->collection( 'series' )->get( 'has_field' ) )
			{
				$cr = $column . $row;
				$sheet->setCellValue( $cr , 'Fält' );
				$column++;
			}
			if ( $state->collection( 'series' )->get( 'has_precision' ) )
			{
				$cr = $column . $row;
				$text = ( $state->collection( 'series' )->get( 'has_field' ) ? 'Precision' : 'Summa' );
				$sheet->setCellValue( $cr , $text );
				$data->crdata->sums_column_end = $column;
				$column++;
				if ( $state->has_handicaps() )
				{
					$cr = $column . $row;
					$sheet->setCellValue( $cr , 'Hkp' );
					$column++;

					$cr = $column . $row;
					$sheet->setCellValue( $cr , 'Totalt' );
					$data->crdata->sums_column_end = $column;
					$column++;
				}
			}

			$row++;
			$column = $start_column;
			$rank = 1;

			$data->crdata->participants_start_row = $row;
			$data->crdata->participants_end_row = $row;

			foreach( $participants as $p )
			{
				$cr = $column . $row;
				$sheet->setCellValue( $cr , $rank );
				$column++;

				$cr = $column . $row;
				$sheet->setCellValue( $cr , $p->name );
				$column++;

				$cr = $column . $row;
				$sheet->setCellValue( $cr , $p->weapon_group );
				$column++;

				foreach( $state->get_series() as $series_index => $the_series )
				{
					// Scores are 1 based.
					$series_index++;

					if ( $series_index > $state->collection( 'series' )->get( 'max' ) )
						break;

					$key = 'score_' . $series_index;
					if ( ! isset( $p->$key ) )
					{
						$column++;
						continue;
					}

					$cr = $column . $row;

					// Here is a score.
					switch( $the_series->type )
					{
						case 'field':
							$key2 = 'score_meta_' . $series_index;
							$score = sprintf( '%s / %s', $p->$key, $p->$key2[ 0 ] );
							break;
						case 'precision':
							if ( $the_series->shots == 5 )
							{
								if ( $p->$key == 50 )
									$sheet
										->getStyle( $cr )
										->applyFromArray( [
											'font' => [
												'color' => [ 'rgb' => 'F04124' ],
											],
										] );
							}
							$score = $p->$key;
					}

					$sheet->setCellValue( $cr , $score );
					$column++;
				}

				if ( $state->collection( 'series' )->get( 'has_field' ) )
				{
					$cr = $column . $row;
					$text = sprintf( '%s / %s', $p->field_hits_total, $p->field_targets_total );
					$sheet->setCellValue( $cr , $text );
					if ( $state->collection( 'series' )->get( 'has_precision' ) )
						$column++;
				}

				if ( $state->collection( 'series' )->get( 'has_precision' ) )
				{
					$cr = $column . $row;
					$sheet->setCellValue( $cr , $p->precision_total );

					if ( $state->has_handicaps() )
					{
						$column++;
						$cr = $column . $row;
						$text = $p->precision_total_with_handicap - $p->precision_total;
						if ( $text == 0 )
							$text = '';
						$sheet->setCellValue( $cr , $text );

						$column++;
						$cr = $column . $row;
						$sheet->setCellValue( $cr , $p->precision_total_with_handicap );
					}
				}

				$max_column = $column;

				$data->crdata->participants_end_row = $row;

				$rank++;
				$row++;
				$column = $start_column;
			}

			// Center the various columns.
			foreach( $columns_to_center as $column_to_center => $from_which_row )
			{
				$key = sprintf( '%s%s:%s%s',
					$data->crdata->$column_to_center,
					$data->crdata->$from_which_row,
					$data->crdata->$column_to_center,
					$data->crdata->participants_end_row
				);
				$sheet
					->getStyle( $key )
					->applyFromArray( [
						'alignment' => [
							'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
						],
					] );
			}

			// Center the series.
			$key = sprintf( '%s%s:%s%s',
				$data->crdata->series_column_start,
				$data->crdata->series_row_start,
				$data->crdata->series_column_end,
				$data->crdata->participants_end_row
			);
			$sheet
				->getStyle( $key )
				->applyFromArray( [
					'alignment' => [
						'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
					],
				] );

			// Center the sums
			$key = sprintf( '%s%s:%s%s',
				$data->crdata->sums_column_start,
				$data->crdata->series_row_start,
				$data->crdata->sums_column_end,
				$data->crdata->participants_end_row
			);
			$sheet
				->getStyle( $key )
				->applyFromArray( [
					'alignment' => [
						'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
					],
				] );

			// Border the table.
			$key = sprintf( '%s%s:%s%s',
				'A',
				last( $data->crdata->headings_rows ),
				$data->crdata->sums_column_end,
				$data->crdata->participants_end_row
			);
			$sheet
				->getStyle( $key )
				->applyFromArray( [
					'borders' => [
						'allBorders' => [
							'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
						],
					],
				] );

			$row++;
		}

		// Add non shooters.
		foreach( $state->participants->collection( 'non_shooting_classes' ) as $class_id => $ignore )
		{
			$participants = $state->participants->collection( 'non_shooting_classes' )->collection( $class_id )->sort_by( function( $p )
			{
				return $p->name;
			} );

			// Insert a newline.
			$cr = $column . $row;
			$sheet->setCellValue( $cr , ' ' );
			$row++;

			$cr = $column . $row;

			$class_name = $classes[ $class_id ];
			$cr = $column . $row;
			$sheet->setCellValue( $cr , $class_name );

			$sheet
				->getStyle( $cr )
				->applyFromArray( [
					'font' => [
						'italic' => true,
					],
				] );
			// Rank / Class
			$column++;

			$column = $start_column;
			$row++;

			foreach( $participants as $participant )
			{
				$cr = $column . $row;
				$sheet->setCellValue( $cr , $participant->name );
				$row++;
			}
		}

		// Merge the first rows in order to allow for tighter column widths.
		$sheet->mergeCells( 'A1:B1' );
		$sheet->mergeCells( 'A2:B2' );
		$sheet->mergeCells( 'A3:B3' );

		// Column name
		$row = 3;
		$sheet->mergeCells( 'C2:' . $data->crdata->sums_column_end . $row );

		$cr = 'C2';
		$sheet->setCellValue( $cr , $meta->get( 'name' ) );
		$sheet
			->getStyle( $cr )
			->applyFromArray( [
				'alignment' => [
					'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
					'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
				],
				'font' => [
					'bold' => true,
					'size' => 14,
				],
			] );

		// Headings row in bold.
		if ( isset( $data->crdata->headings_rows ) )
			foreach( $data->crdata->headings_rows as $heading_row )
				$sheet
					->getStyle( 'A' . $heading_row . ':' . $data->crdata->sums_column_end . $heading_row )
					->applyFromArray( [
						'font' => [
							'bold' => true,
						],
					] );


		// Resize the columns.
		foreach( range( 'A', $max_column ) as $character )
			$sheet->getColumnDimension( $character )->setAutoSize( true );

		if ( ! isset( $data->second_pass ) )
		{
			$data->highest_column = $sheet->getHighestColumn();
			$data->highest_row = $sheet->getHighestRow();
			$data->second_pass = true;
			return $this->get_spreadsheet( $data );
		}

		return $spreadsheet;
	}

	/**
		@brief		Return just the date the comp was created.
		@since		2018-04-26 23:00:48
	**/
	public function get_updated_at_date()
	{
		return preg_replace( '/ .*/', '', $this->updated_at );
	}

	/**
		@brief		Hash this value.
		@details	Generates a 4 char hash. Cheaper than an md5.
		@since		2018-04-08 19:39:51
	**/
	public function hash( $value )
	{
		$hash = md5( json_encode( $value ) );
		$hash = substr( $hash, -4 );
		return $hash;
	}

	/**
		@brief		Maybe create this competition.
		@since		2018-07-26 21:12:00
	**/
	public static function maybe_create( $key )
	{
		$competition = Competition::where( 'key', $key )->first();
		if ( ! $competition )
		{
			$competition = new Competition();
			$competition->key = $key;
			$competition->save();
			$competition->generate_default_meta();

			if ( app()->user() )
				$competition->meta()->set( 'group', app()->user()->group->id );
		}
		return $competition;
	}

	/**
		@brief		Prune this competition and everything related.
		@since		2018-04-24 21:50:34
	**/
	public function prune()
	{
		// Find all participants.
		$participants = Participant::where( 'competition_id', $this->id )->get();
		// Delete each one.
		foreach( $participants as $participant )
			$participant->meta()->delete_all();
		$this->meta()->delete_all();
		$this->delete();
	}

	/**
		@brief		Prune old competitions.
		@since		2018-04-24 21:46:08
	**/
	public static function prune_old()
	{
		$one_week = date( 'Y-m-d', strtotime( '1 week ago' ) );
		$old = static::where( 'created_at', '<', $one_week )->get();
		foreach( $old as $competition )
			$competition->prune();
	}

	/**
		@brief		Generate a random key.
		@since		2018-04-07 22:42:45
	**/
	public static function random_key()
	{
		$r = '';
		for( $counter = 0; $counter < 2; $counter++ )
		{
			$characters = 'bcdfghjkmnpqrstvw';
			$r .= substr( str_shuffle( $characters ), 0, 1 );
			$characters = 'aeou';
			$r .= substr( str_shuffle( $characters ), 0, 1 );
		}
		if ( rand( 0, 100 ) > 50 )
			$r .= rand( 10, 99 );
		return $r;
	}

	/**
		@brief		Sort the participants by their scores.
		@since		2018-05-01 13:12:06
	**/
	public static function sort_participants_by_score( $participants )
	{
		return $participants->sort_by( function( $participant )
		{
			// The minus is so that the biggest is first.
			$r =  - ( $participant->field_hits_total * 400 ) - $participant->precision_total_with_handicap * 200;
			if ( isset( $participant->last_precision ) )
				$r .= $participant->last_precision;
			return $r;
		} );
	}

	/**
		@brief		Switch the competitors of two lanes.
		@since		2018-06-07 12:09:41
	**/
	public function switch_lanes( $from, $to )
	{
		$state = $this->get_state();

		$from_participant = '';
		$from_busy = false;

		$lanes = $state->collection( 'lanes' );
		$lanes_busy = $lanes[ 'busy' ]->toArray();	// Since the objects use string keys, but we're switching with integers. What a mess.
		// We have to find the participant that is busy in this lane.
		if ( isset( $lanes_busy[ $from ] ) )
		{
			$from_participant = $lanes->collection( 'busy' )->get( $from );
			$from_participant = $from_participant[ 'participant_name' ];
			$from_p = $this->get_participant( $from_participant );
			$from_busy = $from_p->meta()->get( 'busy' );
			$from_p->meta()->set( 'lane', $to );
		}

		$to_participant = '';
		if ( isset( $lanes_busy[ $to ] ) )
		{
			$to_participant = $lanes->collection( 'busy' )->get( $to );
			$to_participant = $to_participant[ 'participant_name' ];
			$to_p = $this->get_participant( $to_participant );
			// If the p is busy shooting, then he has to switch lanes. If he's done, then his lane doesn't matter.
			if ( $to_p->meta()->get( 'busy' ) )
				// The person taking this lane must also be busy. People who are done and switch lanes don't matter.
				if ( $from_busy )
					$to_p->meta()->set( 'lane', $from );
		}
	}

	/**
		@brief		Find a queued participant and assign a lane.
		@since		2018-04-16 21:08:37
	**/
	public function unqueue()
	{
		$state = $this->get_state();

		// Find an empty.
		$lanes = $state->collection( 'lanes' )->collection( 'free' );
		if ( count( $lanes ) < 1 )
			return;
		$lane_id = $lanes->first();

		$sorted = $state->participants->collection( 'queued' )->sort_by( function( $p )
		{
			return $p->queued;
		} );
		if ( count( $sorted ) < 1 )
			return;

		$p = $sorted->first();
		$participant = $this->get_participant( $p->name );

		$participant->meta()->set( 'lane', $lane_id );
		$participant->meta()->set( 'busy', true );
		$participant->meta()->delete( 'queued' );
	}

	/**
		@brief		Communicate with the LSF plugin in order to fetch the various competition things.
		@since		2019-04-26 11:04:45
	**/
	public function use_p1_api_key( $key )
	{
		$json = base64_decode( $key );
		$json = json_decode( $json );

		if ( ! is_object( $json ) )
			return;

		$state = $this->get_state();
		$state->p1_api_key = $json;
		$this->p1_api()->fetch_groups();
	}


	/**
		@brief		Verify that the user specified password exists at all.
		@details	Will accept a read or write password.
		@since		2018-07-26 22:26:39
	**/
	public function verify_password( $password )
	{
		$passwords = [
			$this->meta()->get( 'read_password' ),
			$this->meta()->get( 'write_password' ),
		];
		$passwords = array_filter( $passwords );
		if ( count( $passwords ) < 1 )
			return true;
		return in_array( $password, $passwords );
	}
}
