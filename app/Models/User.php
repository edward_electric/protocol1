<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
    	@brief		Does this user have a group?
    	@since		2018-10-06 19:49:33
    **/
    public function has_group()
    {
    	return $this->group_id > 0;
    }

    /**
    	@brief		The group this user belongs to.
    	@since		2018-10-06 19:41:50
    **/
    public function group()
    {
        return $this->BelongsTo( 'App\Models\Group' );
    }
}
