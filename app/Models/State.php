<?php

namespace App\Models;

/**
	@brief		Contains the competition's state information.
	@since		2018-07-07 16:03:19
**/
class State extends \App\Classes\Collection
{
	/**
		@brief		The competition we are derived from.
		@since		2018-07-07 16:03:31
	**/
	public $competition;

	/**
		@brief		Return the actual series data.
		@since		2018-07-08 10:42:17
	**/
	public function get_series()
	{
		return $this->collection( 'series' )->get( 'series' );
	}

	/**
		@brief		Does this state have any handicaps?
		@since		2018-07-08 11:26:15
	**/
	public function has_handicaps()
	{
		$handicaps = $this->collection( 'handicaps' );
		if ( $handicaps->get( 'personal_handicaps' ) )
			return true;
		foreach( $handicaps->get( 'handicaps' ) as $weapon_group => $handicap )
			if ( $handicap != 0 )
				return true;
		return false;
	}

	/**
		@brief		Are personal handicaps enabled?
		@since		2018-09-22 16:07:26
	**/
	public function has_personal_handicaps()
	{
		$handicaps = $this->collection( 'handicaps' );
		return $handicaps->get( 'personal_handicaps' );
	}

	/**
		@brief		Set the competition.
		@since		2018-07-07 16:13:57
	**/
	public function set_competition( Competition $competition )
	{
		$this->competition = $competition;
		return $this;
	}
}
