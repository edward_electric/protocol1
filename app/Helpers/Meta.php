<?php

namespace App\Helpers;

use App\Models\Meta as MetaM;

/**
	@brief		Helps models manipulate their metadata.
	@since		2018-04-12 15:42:49
**/
class Meta
{
	/**
		@brief		The ID of the object we are storing for.
		@since		2018-05-03 21:31:40
	**/
	public $object_id;

	/**
		@brief		The type of object we are storing for.
		@since		2018-04-12 15:43:32
	**/
	public $object_type;

	/**
		@brief		Delete a meta item.
		@since		2018-04-12 17:06:05
	**/
	public function delete( $meta_key )
	{
		MetaM::where( 'object_type', $this->object_type )
			->where( 'object_id', $this->object_id )
			->where( 'meta_key', $meta_key )
			->delete();
	}

	/**
		@brief		Delete all meta for this item.
		@since		2018-04-24 21:51:38
	**/
	public function delete_all()
	{
		MetaM::where( 'object_type', $this->object_type )
			->where( 'object_id', $this->object_id )
			->delete();
	}

	/**
		@brief		Constructor for a model.
		@since		2018-04-12 15:43:03
	**/
	public static function from_model( $model )
	{
		$meta = new static();
		$meta->object_id = $model->id;
		$meta->object_type = preg_replace( '/.*\\\/', '', get_class( $model ) );
		return $meta;
	}

	/**
		@brief		Create a generic meta helper.
		@since		2018-05-03 22:33:02
	**/
	public static function generic( $object_type = 'global', $object_id = '1' )
	{
		$meta = new static();
		$meta->object_id = $object_id;
		$meta->object_type = $object_type;
		return $meta;
	}

	/**
		@brief		Retrieve a collection of all of the meta.
		@since		2018-04-12 15:49:31
	**/
	public function get_all()
	{
		$metas = MetaM::where( 'object_type', $this->object_type )
			->where( 'object_id', $this->object_id )
			->get();
		$r = new \App\Classes\Collection();
		foreach( $metas as $index => $row )
		{
			$value = json_decode( $row->meta_value );
			if ( ! $value )
				$value = $row->meta_value;
			$r->set( $row->meta_key, $value );
		}
		return $r;
	}

	/**
		@brief		Return the meta value for this key.
		@since		2018-04-08 02:02:26
	**/
	public function get( $meta_key, $default = false )
	{
		$meta = MetaM::where( 'object_type', $this->object_type )
			->where( 'object_id', $this->object_id )
			->where( 'meta_key', $meta_key )
			->first();
		if ( ! $meta )
			return $default;
		return $meta->meta_value;
	}

	/**
		@brief		Convenience method to get a json object from the meta.
		@since		2018-04-08 02:23:01
	**/
	public function get_json( $key )
	{
		$json = $this->get( $key );
		return json_decode( $json );
	}

	/**
		@brief		Set a meta value.
		@since		2018-04-08 02:18:07
	**/
	public function set( $key, $value )
	{
		$meta = MetaM::where( 'object_type', $this->object_type )
			->where( 'object_id', $this->object_id )
			->where( 'meta_key', $key )
			->first();

		if ( is_array( $value ) )
			$value = json_encode( $value );
		if ( is_object( $value ) )
			$value = json_encode( $value );

		if ( ! $meta )
		{
			$meta = new MetaM();
			$meta->meta_key = $key;
			$meta->meta_value = $value;
			$meta->object_type = $this->object_type;
			$meta->object_id = $this->object_id;
			$meta->save();
		}
		else
		{
			$meta->meta_value = $value;
			$meta->save();
		}
		return $this;
	}

	/**
		@brief		Convenience method to set a json object as the meta.
		@since		2018-04-08 02:23:40
	**/
	public function set_json( $key, $value )
	{
		return $this->set( $key, json_encode( $value ) );
	}
}