<?php

/**
	@brief		Create a collection.
	@since		2018-07-08 10:30:50
**/
function collection( $items = [] )
{
	return new \App\Classes\Collection( $items );
}

/**
	@brief		Return the Alerts helper.
	@since		2018-07-12 22:49:12
**/
function alerts()
{
	$r = new \App\Helpers\Alerts();
	$r->load();
	return $r;
}
