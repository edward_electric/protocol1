<?php

namespace App\Classes;

/**
	@brief		Convenience class so we don't have to call the SDK class directly.
	@since		2018-04-12 09:53:20
**/
class Collection
	extends \plainview\sdk\collections\collection
{
}