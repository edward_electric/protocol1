<?php

namespace App\Classes;

/**
	@brief		Base class for app containing a bunch of useful features.
	@since		2018-12-30 15:07:01
**/
class App
	extends \Illuminate\Foundation\Application
{
	/**
		@brief		Create a new collection
		@since		2018-12-30 21:26:32
	**/
	public function collection( $items = [] )
	{
		return new Collection( $items );
	}

    /**
    	@brief		Return a Plainview Form object.
    	@since		2018-11-13 13:09:48
    **/
    public static function form()
    {
    	$form = new \plainview\sdk\form2\form();
		$form->enctype( 'multipart/form-data' );
		return $form;
    }

	/**
		@brief		Return the logger.
		@since		2018-11-16 16:57:59
	**/
	public function log()
	{
		return new Logger();
	}

	/**
		@brief		Return the currently logged in user.
		@since		2019-01-08 19:34:54
	**/
	public function user()
	{
		return \Auth::user();
	}
}
