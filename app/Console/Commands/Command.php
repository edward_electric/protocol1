<?php

namespace App\Console\Commands;

/**
	@brief		Base class for Artisan commands.
	@since		2017-12-17 22:24:06
**/
class Command
	extends \Illuminate\Console\Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = '-';
}
