<?php

namespace App\Console\Commands\User;

use App\Models\User;

/**
	@brief		Show the user.
	@since		2018-10-20 19:25:09
**/
class Show
	extends \App\Console\Commands\Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:user:show { user_id : The ID of the user to display. }';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Display a user.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
    	$user_id = $this->argument( 'user_id' );

    	// Find the user with this name.
    	$user = User::findOrFail( $user_id );

    	ddd( json_encode( $user ) );
    }
}
