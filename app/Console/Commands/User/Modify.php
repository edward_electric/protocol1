<?php

namespace App\Console\Commands\User;

use App\Models\User;
use Illuminate\Support\Facades\Hash;

/**
	@brief		Modify a property of a user.
	@since		2018-10-20 19:26:41
**/
class Modify
	extends \App\Console\Commands\Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:user:modify { user_id : The ID of the user to modify. } { key : The user key to modify. } { value : The new value for the key. }';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Modify a user.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
    	$user_id = $this->argument( 'user_id' );
    	$key = $this->argument( 'key' );
    	$value = $this->argument( 'value' );

    	// Find the user with this name.
    	$user = User::findOrFail( $user_id );

    	switch( $key )
    	{
    		case 'password':
    			$user->password = Hash::make( $value );
    		break;
    		default:
    			$user->$key = $value;
    		break;
    	}

    	$user->save();
    }
}
