<?php

namespace App\Console\Commands\User;

use App\Models\User;
use Illuminate\Support\Facades\Hash;

/**
	@brief		Create a user.
	@since		2018-10-20 19:19:07
**/
class Create
	extends \App\Console\Commands\Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = "app:user:create { email : The user's e-mail address. }";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new user.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
    	$email = $this->argument( 'email' );

    	$user = new User();
    	$user->name = microtime();
    	$user->password = Hash::make( microtime() );
    	$user->email = $email;
    	$user->save();

    	ddd( 'User %s created.', $user->id );
    }
}
