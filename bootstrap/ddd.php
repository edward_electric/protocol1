<?php

if ( ! function_exists( 'ddd' ) )
{
	function ddd()
	{
		// Convert the non-string arguments into lovely code blocks.
		$args = func_get_args();
		foreach( $args as $index => $arg )
		{
			$export = false;
			$export |= is_bool( $arg );
			$export |= is_array( $arg );
			$export |= is_object( $arg );
			if ( $export )
			{
				ob_start();
				var_dump( $arg );
				$a = ob_get_contents();
				ob_end_clean();
				$args[ $index ] = $a;
			}
		}

		$text = $args[ 0 ];
		if ( strpos( $text, '%' ) !== false )
		{
			// Put all of the arguments into one string.
			$text = @call_user_func_array( 'sprintf', $args );
			if ( $text == '' )
				$text = $args[ 0 ];
		}
		else
			$text = implode( "<br/>\n", $args );

		$time = microtime( true );
		$seconds = preg_replace( '/\..*/', '', $time );
		$microseconds = preg_replace( '/.*\./', '', $time );
		$microseconds = str_pad( $microseconds, 4, '0' );
		$timestamp = sprintf( '%s.%s', date( 'Y-m-d H:i:s', $seconds ), $microseconds );
		$text = sprintf( '%s %s%s', $timestamp, $text, "\n" );

		$http = isset( $_SERVER[ 'REMOTE_ADDR' ] );
		if ( $http )
			$text = sprintf( '<pre>%s</pre>', $text );
		echo $text;
		if ( $http )
			ob_flush();
	}
}
